//
//  Favorites_ViewController.swift
//  FanClub
//
//  Created by Brst on 5/18/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore
import SDWebImage

class Favorites_ViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{

    @IBOutlet var back_btn: UIButton!
    @IBOutlet var collectionView2: UICollectionView!
    @IBOutlet var collectionView1: UICollectionView!
    
    var arrayImg = NSMutableArray()
    var arrayTeams = NSMutableArray()
    var arrayNFL = NSMutableArray()
    var arrayNBA = NSMutableArray()
    var arrayMLB = NSMutableArray()
    var arrayNCCA = NSMutableArray()
    var checkFavorites = Bool()
    var data = Int()
    var str = String()
    var cell1 : Favorites_TeamCollectionViewCell!
    var cell : Favorites_CollectionViewCell!
    var getNFL = NSMutableArray()
    var getNBA = NSMutableArray()
    var getMLB = NSMutableArray()
    var getNCCA = NSMutableArray()
    var gotData = Bool()
    var str1 = String()
    var uid = String()
    
//MARK: ***View Did Load Method :-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if checkFavorites == true
        {
            back_btn.isHidden = false
            self.tabBarController?.tabBar.isHidden = true;
            self.collectionView1.frame.size.height = self.collectionView1.frame.size.height + (self.tabBarController?.tabBar.frame.size.height)!
            self.collectionView1.sizeToFit()
        }
        
        collectionView1.allowsSelection = true;
        collectionView1.allowsMultipleSelection = false;
        let img1 = UIImage(named: "1111")
        let img3 = UIImage(named: "3333")
        let img4 = UIImage(named: "4444")
        let img5 = UIImage(named: "5555")
        arrayImg = [img3!, img1!, img5!, img4!]
        
        arrayTeams = ["NFL", "NBA", "MLB", "NCCA Football"]
        arrayNFL = ["Arizona Cardinals", "Atlanta Falcons", "Baltimore Ravens", "Buffalo Bills", "Carolina Panthers", "Chicago Bears", "Cincinnati Bengals", "Cleveland Browns", "Dallas Cowboys", "Denver Broncos", "Detroit Lions", "Green Bay Packers", "Houston Texans", "Indianapolis Colts", "Jacksonville Jaguars", "Kansas City Chiefs", "Miami Dolphins", "Minnesota Vikings", "New England Patriots", "New Orleans Saints", "New York Giants", "New York Jets ", "Oakland Raiders", "Philadelphia Eagles", "Pittsburgh Steelers", "San Diego Chargers", "San Francisco 49ers", "Seattle Seahawks", "Los Angeles Rams", "Tampa Bay Buccaneers", "Tennessee Titans"]
        arrayNBA = ["Atlanta Hawks", "Boston Celtics", "Charlotte Hornets", "Chicago Bulls", "Cleveland Cavaliers", "Dallas Mavericks", "Denver Nuggets", "Detroit Pistons", "Golden State Warriors", "Houston Rockets", "Indiana Pacers", "Los Angeles Clippers", "Los Angeles Lakers", "Memphis Grizzlies", "Miami Heat", "Milwaukee Bucks", "Minnesota Timberwolves", "New Jersey Nets", "New Orleans Pelicans", "New York Knicks", "Oklahoma City Thunder", "Orlando Magic", "Philadelphia Sixers", "Phoenix Suns", "Portland Trail Blazers", "Sacramento Kings", "San Antonio Spurs", "Toronto Raptors", "Utah Jazz", "Washington Wizards"]
        arrayMLB = ["Arizona Diamondbacks", "Atlanta Braves", "Baltimore Orioles", "Boston Red Sox", "Chicago Cubs", "Chicago White Sox", "Cincinnati Reds", "Cleveland Indians", "Colorado Rockies", "Detroit Tigers", "Miami Marlins", "Houston Astros", "Kansas City Royals", "Los Angeles Angels of Anaheim", "Los Angeles Dodgers", "Milwaukee Brewers", "Minnesota Twins", "New York Mets", "New York Yankees", "Oakland Athletics", "Philadelphia Phillies", "Pittsburgh Pirates", "Saint Louis Cardinals", "San Diego Padres", "San Francisco Giants", "Seattle Mariners", "Tampa Bay Rays", "Texas Rangers", "Toronto Blue Jays", "Washington Nationals"]
        arrayNCCA = ["Air Force Falcons", "Akron Zips", "Alabama Crimson Tide", "Alabama-Birmingham Blazers", "Arizona Wildcats", "Arizona State Sun Devils", "Arkansas Razorbacks", "Arkansas State Indians", "Army Black Knights", "Auburn Tigers", "Ball State Cardinals", "Baylor Bears", "Boise State Broncos", "Boston College Eagles", "Bowling Green Falcons", "Brigham Young Cougars", "Buffalo Bulls", "California Golden Bears", "Central Florida Knights", "Central Michigan Chippewas", "Cincinnati Bearcats", "Clemson Tigers", "Colorado Buffaloes", "Colorado State Rams", "Connecticut Huskies", "Duke Blue Devils", "East Carolina Pirates", "Eastern Michigan Eagles", "Florida Gators", "Florida Atlantic Owls", "Florida International Golden Panthers","Washington Redskins","Florida State Seminoles","Fresno State Bulldogs","Georgia Bulldogs","Georgia Tech Yellow Jackets","Hawaii Warriors","Houston Cougars","Idaho Vandals","Illinois Fighting Illini","Indiana Hoosiers","Iowa Hawkeyes","Iowa State Cyclones","Kansas Jayhawks","Kansas State Wildcats","Kent State Golden Flashes","Kentucky Wildcats","Louisiana State Tigers","Louisiana Tech Bulldogs","Louisiana-Lafayette Ragin' Cajuns","Louisiana-Monroe Warhawks","Louisville Cardinals","Marshall Thundering Herd","Maryland Terrapins","Memphis Tigers","Miami (FL) Hurricanes","Miami (OH) RedHawks","Michigan Wolverines","Michigan State Spartans","Middle Tennessee State Blue Raiders","Minnesota Golden Gophers","Mississippi Rebels","Mississippi State Bulldogs","Missouri Tigers","Navy Midshipmen","Nebraska Cornhuskers","Nevada Wolf Pack","Nevada-Las Vegas Runnin' Rebels","New Mexico Lobos","New Mexico State Aggies","North Carolina Tar Heels","North Carolina State Wolfpack","North Texas Mean Green","Northern Illinois Huskies","Northwestern Wildcats","Notre Dame Fighting Irish","Ohio Bobcats","Ohio State Buckeyes","Oklahoma Sooners","Oklahoma State Cowboys","Oregon Ducks","Oregon State Beavers","Penn State Nittany Lions","Pittsburgh Panthers","Purdue Boilermakers","Rice Owls","Rutgers Scarlet Knights","San Diego State Aztecs","San Jose State Spartans","South Carolina Gamecocks","South Florida Bulls","Southern California Trojans","Southern Methodist Mustangs","Southern Mississippi Golden Eagles","Stanford Cardinal","Syracuse Orange","Temple Owls","Tennessee Volunteers","Texas Longhorns","Texas A&M Aggies","Texas Christian Horned Frogs","Texas Tech Red Raiders","Texas-El Paso Miners","Toledo Rockets","Troy Trojans","Tulane Green Wave","Tulsa Golden Hurricane","UCLA Bruins","Utah Utes","Utah State Aggies","Vanderbilt Commodores","Virginia Cavaliers","Virginia Tech Hokies","Wake Forest Demon Deacons","Washington Huskies","Washington State Cougars","West Virginia Mountaineers","Western Kentucky Hilltoppers","Western Michigan Broncos","Wisconsin Badgers","Wyoming Cowboys"]
            UserDefaults.standard.set(0, forKey: "dataDef")

    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.GettingFavorties()
    }
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
//MARK: Collection View Functions here :-
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionView2    {
            return arrayImg.count   }
        else
        {
            if UserDefaults.standard.object(forKey: "dataDef") != nil
            {
             data = UserDefaults.standard.object(forKey: "dataDef") as! Int
            if data == 0    {
                return arrayNFL.count
            }
            if data == 1    {
                return arrayNBA.count
            }
            if data == 2    {
                return arrayMLB.count
            }
            if data == 3    {
                return arrayNCCA.count
            }
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionView2
        {
            cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! Favorites_TeamCollectionViewCell
            cell1.layer.cornerRadius = 5;
            cell1.layer.masksToBounds = true
            cell1.layer.borderWidth = 1.0
            cell1.layer.borderColor = UIColor.lightGray.cgColor
            cell1.img.image = arrayImg[indexPath.row] as? UIImage
            cell1.lbl.text = arrayTeams[indexPath.row] as? String
            if UserDefaults.standard.object(forKey: "dataDef") != nil   {
            if indexPath.row == UserDefaults.standard.object(forKey: "dataDef") as! Int
            {
                cell1.layer.cornerRadius = 5;
                cell1.layer.masksToBounds = true
                cell1.layer.borderWidth = 1.5
                cell1.layer.borderColor = UIColor.red.cgColor
                cell1.img.image = arrayImg[indexPath.row] as? UIImage
                }
            }
           
            return cell1
        }
        else
        {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Favorites_CollectionViewCell
            cell.layer.cornerRadius = 5;
            cell.layer.masksToBounds = true
            cell.layer.borderWidth = 1.0
            cell.layer.borderColor = UIColor.lightGray.cgColor
            
            if UserDefaults.standard.object(forKey: "dataDef") != nil
                {
                    
                    //self.GettingFavorties()
                    self.data = UserDefaults.standard.object(forKey: "dataDef") as! Int
                    if self.data == 0
                    {
                        str1 = (self.arrayNFL[indexPath.row] as? String)!
                        if getNFL.contains(str1)
                        {
                            self.cell.img.image = UIImage(named: "star-1")
                            cell.lbl.text = str1
                        }
                        else
                        {
                            self.cell.img.image = UIImage(named: "star2")
                            cell.lbl.text = str1
                        }
                    }
                    if self.data == 1
                    {
                        str1 = (self.arrayNBA[indexPath.row] as? String)!
                        if getNBA.contains(str1)
                        {
                            self.cell.img.image = UIImage(named: "star-1")
                            cell.lbl.text = str1
                        }
                        else
                        {
                            self.cell.img.image = UIImage(named: "star2")
                            cell.lbl.text = str1
                        }
                    }
                    if self.data == 2
                    {
                        str1 = (self.arrayMLB[indexPath.row] as? String)!
                        if getMLB.contains(str1)
                        {
                            self.cell.img.image = UIImage(named: "star-1")
                            cell.lbl.text = str1
                        }
                        else
                        {
                            self.cell.img.image = UIImage(named: "star2")
                            cell.lbl.text = str1
                        }
                    }
                    if self.data == 3
                    {
                        str1 = (self.arrayNCCA[indexPath.row] as? String)!
                        if getNCCA.contains(str1)
                        {
                            self.cell.img.image = UIImage(named: "star-1")
                            cell.lbl.text = str1
                        }
                        else
                        {
                            self.cell.img.image = UIImage(named: "star2")
                            cell.lbl.text = str1
                        }
                    }
                }
                return cell
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let flowLayout: UICollectionViewFlowLayout? = (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)
        let cellWidth: CGFloat = collectionView.frame.width / 5
        flowLayout?.minimumInteritemSpacing = 5
        flowLayout?.minimumLineSpacing = cellWidth/3
        flowLayout?.itemSize = CGSize(width: cellWidth, height: cellWidth)
        return (flowLayout?.itemSize)!
    }
//MARK: ##Collection View Did Select Function :-
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if collectionView == collectionView2
        {
            UserDefaults.standard.set(indexPath.row, forKey: "dataDef")
            UserDefaults.standard.set(cell1.lbl.text, forKey: "teamName")
            collectionView2.reloadData()
            collectionView1.reloadData()
        }
        else
        {
            let cell11 = collectionView.cellForItem(at: indexPath) as! Favorites_CollectionViewCell!
            cell11?.layer.borderWidth = 1.3
            cell11?.layer.borderColor = UIColor.red.cgColor
            cell11?.img.image = UIImage(named: "star-1")
           
            // Selecting Team for Party inVitation
            if checkFavorites == true
            {
                if #available(iOS 9.0, *) {
                    self.tabBarController?.tabBar.isHidden = false;
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "create_watchParty") as! Create_WatchParty_ViewController
                    checkFavorites = false
                    vc.checkWatchParty = true
                    UserDefaults.standard.set(cell11?.lbl.text, forKey: "selectedTeam")
                    self.navigationController?.popViewController(animated: true)
                    
                }
                else
                {
                    print("nothing to show")
            }
            }
            else
            {
                if UserDefaults.standard.object(forKey: "dataDef") != nil
                {
                    data = UserDefaults.standard.object(forKey: "dataDef") as! Int
                    if data == 0
                    {
                        str = arrayNFL[indexPath.row] as! String
                        if getNFL.contains(str)
                        {
                            getNFL.remove(str)
                            self.removeTeam()
                        }
                        else
                        {
                            getNFL.add(str)
                            self.addTeam()
                        }
                    }
                    if data == 1
                    {
                        str = arrayNBA[indexPath.row] as! String
                        if getNBA.contains(str)
                        {
                            getNBA.remove(str)
                            self.removeTeam()
                        }
                        else{
                            getNBA.add(str)
                            self.addTeam()

                        }
                    }
                    if data == 2
                    {
                        str = arrayMLB[indexPath.row] as! String
                        if getMLB.contains(str)
                        {
                            getMLB.remove(str)
                            self.removeTeam()
                        }
                        else{
                            getMLB.add(str)
                            self.addTeam()
                        }
                    }
                    if data == 3
                    {
                        str = arrayNCCA[indexPath.row] as! String
                        if getNCCA.contains(str)
                        {
                            getNCCA.remove(str)
                            self.removeTeam()
                        }
                        else{
                            getNCCA.add(str)
                            self.addTeam()
                        }
                    }
                }
                self.favorties()
                self.collectionView1.reloadData()

            }
        }
    }
//MARK: Saving Favorties in Firebase
    func favorties()
    {
        let ref = Database.database().reference()
        ref.child("Sports").child((Auth.auth().currentUser?.uid)!).updateChildValues(["NFL": self.getNFL, "NBA": self.getNBA, "MLB": self.getMLB, "NCCA_Football": self.getNCCA])
    }
//MARK: Getting Favorties from Firebase
    func GettingFavorties()
    {
        self.getNFL.removeAllObjects()
        self.getNBA.removeAllObjects()
        self.getNCCA.removeAllObjects()
        self.getMLB.removeAllObjects()
        let ref = Database.database().reference()
        ref.child("Sports").child((Auth.auth().currentUser?.uid)!).observe( .value, with: { (snapshot)in
            if snapshot.exists()
                    {
                        let postDict = snapshot.value as! [String : AnyObject]
                        
                        let array = postDict["NFL"] as? NSMutableArray
                        if array == nil
                        {
                            
                        }
                        else
                        {
                            self.getNFL = NSMutableArray(array: array!)
                        }
                        
                        let array1 = postDict["NBA"] as? NSMutableArray
                        if array1 == nil
                        {
                            
                        }
                        else
                        {
                            self.getNBA = NSMutableArray(array: array1!)
                        }
                        
                        let array2 = postDict["MLB"] as? NSMutableArray
                        if array2 == nil
                        {
                            
                        }
                        else
                        {
                            self.getMLB = NSMutableArray(array: array2!)
                        }
                        
                        let array3 = postDict["NCCA_Football"] as? NSMutableArray
                        if array3 == nil
                        {
                            
                        }
                        else
                        {
                            self.getNCCA = NSMutableArray(array: array3!)
                        }
                        
                        self.collectionView1.reloadData()
                    }
                    else
                    {
                        
                    }
            })
    }
//MARK: Removing user form Team
    func removeTeam()
    {
        let userID = Auth.auth().currentUser?.uid
        let ref = Database.database().reference()
        ref.child("Favourites").child(self.str).child(userID!).removeValue(completionBlock: {  error in
                   })
        
    }
//MARK: Adding user into the team
    func addTeam()
    {
        let userID = Auth.auth().currentUser?.uid
        let ref = Database.database().reference()
        ref.child("Favourites").child(self.str).child(userID!).updateChildValues(["User": "Following the Team"])
    }
   
    @IBAction func btn_backToSelectedTeam(_ sender: Any)
    {
        checkFavorites = false
        if #available(iOS 9.0, *) {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "create_watchParty") as! Create_WatchParty_ViewController
            vc.checkWatchParty = true
        } else {
        }
        UserDefaults.standard.removeObject(forKey: "selectedTeam")
        self.navigationController?.popViewController(animated: true)
    }

}







