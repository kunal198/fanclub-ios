//
//  ProfileViewController.swift
//  FanClub
//
//  Created by Brst on 5/16/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore
import FBSDKCoreKit
import FBSDKLoginKit
import SDWebImage

class ProfileViewController: UIViewController , UIPageViewControllerDelegate, UIPageViewControllerDataSource{
    @IBOutlet weak var view_bio: UIView!
    @IBOutlet weak var view_favorites: UIView!
    @IBOutlet weak var view_attending: UIView!
    @IBOutlet weak var view_invented: UIView!
    @IBOutlet var vw: UIView!
    @IBOutlet var txt_name: UILabel!
    @IBOutlet var txt_email: UILabel!
    @IBOutlet var img_profile: UIImageView!
    @IBOutlet var btn_img_profile: UIButton!

    @IBOutlet weak var containerView: UIView!
        
    @IBOutlet var lbl_invited: UILabel!
    @IBOutlet var img_signOut: UIImageView!
    @IBOutlet var lbl_signOut: UILabel!
    @IBOutlet var btn_signOut_click: UIButton!
    @IBOutlet var lbl_edit: UILabel!
    @IBOutlet var btn_editclick: UIButton!
    @IBOutlet var img_edit: UIImageView!
    @IBOutlet var img_email: UIImageView!
    @IBOutlet var lbl_inviteBadge: UILabel!
    @IBOutlet var lbl_usersPoints: UILabel!
    
    var checkprofile = false
    var checkAttending = false
    var userID = String()
    
    var pageViewController: UIPageViewController?
    var chart1 : UIViewController!
    var chart2 : UIViewController!
    var chart3 : UIViewController!
    var chart4 : UIViewController!
    //var loadingIndicator = UIActivityIndicatorView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  print(FBSDKAccessToken.current())
        self.userID = (Auth.auth().currentUser?.uid)!
        UserDefaults.standard.set(self.userID, forKey: "userIdCheck") // for app delegate use only 
        self.addToken()
        self.tabBarController?.tabBar.isHidden = false
        self.lbl_inviteBadge.layer.cornerRadius = self.lbl_inviteBadge.frame.size.width/2
        self.lbl_inviteBadge.layer.borderWidth = 1
        
        self.lbl_inviteBadge.layer.borderColor = UIColor.init(red: 192/255, green: 57/255, blue: 43/255, alpha: 1).cgColor
        
        // Page View Controller with StoryBoard Addresses
        vw.layer.cornerRadius = 5;
        vw.layer.masksToBounds = true
        vw.layer.borderWidth = 1.0
        vw.layer.borderColor = UIColor.white.cgColor
        
        chart1 = storyboard?.instantiateViewController(withIdentifier: "profile_bio") as? Profile_BioViewController
        
        chart2 =  storyboard?.instantiateViewController(withIdentifier: "profile_attending") as? Profile_AttendingViewController
        chart3 =  storyboard?.instantiateViewController(withIdentifier: "profile_invited") as? Profile_InvitedViewController
        
        chart4 =  storyboard?.instantiateViewController(withIdentifier: "profile_favorites") as? Profile_FavoritesViewController
        
        // print(chart4!,chart3!,chart2!)
        let controllersArray = [chart1!]
        
        pageViewController = storyboard?.instantiateViewController(withIdentifier: "pageViewController") as? UIPageViewController
        self.pageViewController?.delegate = self
        self.pageViewController?.dataSource = self
        self.pageViewController?.setViewControllers(controllersArray as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        addChildViewController(pageViewController!)
        containerView.addSubview((pageViewController?.view)!)
        pageViewController?.didMove(toParentViewController: self)
      //  self.loadingIndicator.stopAnimating()
      //  self.loadingIndicator.hidesWhenStopped = true
       // self.gettingDetails()
        
        let notificationName = Notification.Name("InviteBadgeCountcheck")
        
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.gettingDetails), name: notificationName, object: nil)
        
        let notificationName1 = Notification.Name("addToken")
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.addToken), name: notificationName1, object: nil)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        // chat screen option on Attending Party = true
        UserDefaults.standard.set(checkAttending, forKey: "checkAttending")
        
        //MARK: Container Height for Table view
        
        // shwoing user's profile from Teams screen
        if self.checkprofile == true
        {
            self.containerView.frame.size.height = 40 + self.containerView.frame.size.height
            let viewHeight = self.containerView.frame.size.height
          //  print("Current table View Height = \(viewHeight)")

            UserDefaults.standard.set(viewHeight, forKey: "check")
            UserDefaults.standard.synchronize()
            
            self.img_edit.isHidden = true
            self.lbl_edit.isHidden = true
            self.btn_editclick.isHidden = true
            self.lbl_signOut.isHidden = true
            self.tabBarController?.tabBar.isHidden = true
            self.img_signOut.image = UIImage(named: "back_white")
            self.lbl_invited.text = "Events"
            self.img_email.isHidden = true
            self.txt_email.isHidden = true
            
        }
        else
        {
            self.tabBarController?.tabBar.isHidden = false
            
            let viewHeight = self.containerView.frame.size.height
           // print("Current View Height = \(viewHeight)")

            UserDefaults.standard.set(viewHeight, forKey: "check")
            UserDefaults.standard.synchronize()
        }
        self.gettingDetails()
    }
    override func viewWillDisappear(_ animated: Bool) {
        Database.database().reference().removeAllObservers()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let currentview: UIViewController? = (pageViewController.viewControllers?[0])
        if (currentview is Profile_AttendingViewController) {
            let pvc: Profile_BioViewController? = storyboard?.instantiateViewController(withIdentifier: "profile_bio") as? Profile_BioViewController
                        return pvc
        }
        if (viewController is Profile_InvitedViewController) {
            let pvc: Profile_AttendingViewController? = storyboard?.instantiateViewController(withIdentifier: "profile_attending") as? Profile_AttendingViewController
            return pvc
        }
        if (viewController is Profile_FavoritesViewController) {
            
            let pvc: Profile_InvitedViewController? = storyboard?.instantiateViewController(withIdentifier: "profile_invited") as? Profile_InvitedViewController
            return pvc
        }

        else {
            return nil
        }
    }
   
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        if (viewController is Profile_BioViewController) {
            let tvc: Profile_AttendingViewController? = storyboard?.instantiateViewController(withIdentifier: "profile_attending") as? Profile_AttendingViewController
            return tvc

        }
        if (viewController is Profile_FavoritesViewController) {
            let tvc: Profile_BioViewController? = storyboard?.instantiateViewController(withIdentifier: "profile_bio") as? Profile_BioViewController
                        return tvc
        }
        if (viewController is Profile_InvitedViewController) {
            let tvc: Profile_FavoritesViewController? = storyboard?.instantiateViewController(withIdentifier: "profile_favorites") as? Profile_FavoritesViewController
                        return tvc
        }
        if (viewController is Profile_AttendingViewController) {
            let tvc: Profile_InvitedViewController? = storyboard?.instantiateViewController(withIdentifier: "profile_invited") as? Profile_InvitedViewController
            return tvc
        }

        else {
            return nil
        }
    }
    public func presentationCount(for pageViewController: UIPageViewController) -> Int // The number of items reflected in the page indicator.
    {
        return 4
    }
    public func presentationIndex(for pageViewController: UIPageViewController) -> Int // The selected item reflected in the page indicator.
    {
        return 0
    }
    
   
    
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        let currentview: UIViewController? = (pageViewController.viewControllers?[0])
        
        if (currentview is Profile_BioViewController)
        {
            view_bio.isHidden = false
            view_attending.isHidden = true
            view_invented.isHidden = true
            view_favorites.isHidden = true
        }
        if (currentview is Profile_AttendingViewController)
        {
            UserDefaults.standard.set("attended", forKey: "currentView")
            view_bio.isHidden = true
            view_attending.isHidden = false
            view_invented.isHidden = true
            view_favorites.isHidden = true
        }
        if (currentview is Profile_InvitedViewController)
        {
            UserDefaults.standard.set("invited", forKey: "currentView")
            view_bio.isHidden = true
            view_attending.isHidden = true
            view_invented.isHidden = false
            view_favorites.isHidden = true
        }
        if (currentview is Profile_FavoritesViewController)
        {
            view_bio.isHidden = true
            view_attending.isHidden = true
            view_invented.isHidden = true
            view_favorites.isHidden = false
        }
    }
    @IBAction func profile_favorites_value(_ sender: Any)
    {
        view_bio.isHidden = true
        view_attending.isHidden = true
        view_invented.isHidden = true
        view_favorites.isHidden = false
        
        let controllersArray = [chart4!]
        self.pageViewController?.setViewControllers(controllersArray as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }
    @IBAction func profile_invited_value(_ sender: Any)
    {
        UserDefaults.standard.set("invited", forKey: "currentView")
        view_bio.isHidden = true
        view_attending.isHidden = true
        view_invented.isHidden = false
        view_favorites.isHidden = true
        
        let controllersArray = [chart3!]
        self.pageViewController?.setViewControllers(controllersArray as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }
    @IBAction func profile_attending_value(_ sender: Any)
    {
        UserDefaults.standard.set("attended", forKey: "currentView")
        view_bio.isHidden = true
        view_attending.isHidden = false
        view_invented.isHidden = true
        view_favorites.isHidden = true
        
        let controllersArray = [chart2!]
        self.pageViewController?.setViewControllers(controllersArray as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }
    @IBAction func profile_bio_value(_ sender: Any)
    {
        view_bio.isHidden = false
        view_attending.isHidden = true
        view_invented.isHidden = true
        view_favorites.isHidden = true

        let controllersArray = [chart1!]
        self.pageViewController?.setViewControllers(controllersArray as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }
//MARK:*** Edit Profile Button
    @IBAction func edit_profile_value(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "editProfile") as! Edit_ProfileViewController
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
//MARK: *** Sign Out Button
    @IBAction func sign_out(_ sender: Any)
    {
        if self.checkprofile == true
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            let alert:UIAlertController=UIAlertController(title: "Alert", message: "Are You Sure To Sign Out", preferredStyle: UIAlertControllerStyle.alert)
            let loginAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                // Removing Device Token
               // Database.database().reference().child("users").child((Auth.auth().currentUser?.uid)!).updateChildValues(["DeviceToken": ""])
                // Removing user's session
                Database.database().reference().child((Auth.auth().currentUser?.uid)!).removeAllObservers()

                try! Auth.auth().signOut()
                let loginManager = FBSDKLoginManager()
                loginManager.logOut()
                
                UserDefaults.standard.removeObject(forKey: "userID")
                UserDefaults.standard.removeObject(forKey: "name")
                UserDefaults.standard.removeObject(forKey: "email")
                UserDefaults.standard.removeObject(forKey: "image")
                UserDefaults.standard.removeObject(forKey: "gender")
                UserDefaults.standard.removeObject(forKey: "bio")
                UserDefaults.standard.removeObject(forKey: "phone")
                UserDefaults.standard.removeObject(forKey: "firstname")
                UserDefaults.standard.removeObject(forKey: "lastname")
                // UserDefaults.standard.removeObject(forKey: "dataDef")
                UserDefaults.standard.object(forKey: "dictData")
                
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let walkthroughVC = storyboard.instantiateViewController(withIdentifier: "home") as! ViewController
                let navigationController = self.view.window?.rootViewController as! UINavigationController
                
                navigationController.pushViewController(walkthroughVC, animated: false)
                
              /*  let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "home") as! ViewController
               // self.view.window?.rootViewController = nextViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)*/
            }
            let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
            }
            alert.addAction(loginAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
  
    }
    
    func gettingDetails()
    {
     //   loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
     //   loadingIndicator.center = self.view.center
     //   loadingIndicator.color = UIColor.black
     //   self.loadingIndicator.startAnimating()
    //    self.view.addSubview(self.loadingIndicator)
        
        img_profile.layer.cornerRadius =  self.img_profile.frame.width/2
        img_profile.layer.masksToBounds = true
        img_profile.layer.borderWidth = 1.0
        img_profile.layer.borderColor = UIColor.lightGray.cgColor
        btn_img_profile.layer.cornerRadius = self.btn_img_profile.frame.width/2
        btn_img_profile.layer.masksToBounds = true
        
        UserDefaults.standard.set(self.checkprofile, forKey: "checkProfile")
        var userID = (Auth.auth().currentUser?.uid)!
       // print("UserId = \(userID)")
        if self.checkprofile == true
        {
            userID = UserDefaults.standard.object(forKey: "userId") as! String
            let notificationName = Notification.Name("checkProfile")
           
            NotificationCenter.default.post(name: notificationName, object: nil)

        }
        let refo = Database.database().reference()
        refo.child("users").child(userID).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists()
            {
               // print(snapshot)
                let postDict = snapshot.value as! [String : AnyObject]
                
                let firstName = postDict["First Name"]
                let lastName = postDict["Last Name"]
                let email = postDict["Email"]
                let profileImg = postDict["Profile Image"]
                
                self.txt_email.text = email as? String
                self.txt_name.text =  "\(String(describing: firstName!)) \(String(describing: lastName!))"
                // for chatting screen !!
                UserDefaults.standard.set(self.txt_name.text, forKey: "CurrentUserName")
                self.img_profile.sd_setShowActivityIndicatorView(true)
                self.img_profile.sd_setIndicatorStyle(.gray)
                self.img_profile.sd_setImage(with: URL(string: profileImg as! String), placeholderImage: UIImage(named: "man_2"))
                // Creating InviteBadges here !!!
                let roomKeys = Array(postDict.keys)
                if roomKeys.contains("InviteBadge")
                {
                    let count = postDict["InviteBadge"] as! Int
                  //  UIApplication.shared.applicationIconBadgeNumber = count
                    if count != 0
                    {
                        self.lbl_inviteBadge.text = String(count)
                        self.lbl_inviteBadge.isHidden = false
                    }
                    else
                    {
                        self.lbl_inviteBadge.isHidden = true
                    }
                   
                }
                else
                {
                    refo.child("users").child(userID).updateChildValues(["InviteBadge": 0]) // added inviteBadge = 0
                }
                if roomKeys.contains("Points")
                {
                    let points = postDict["Points"] as! Int
                    self.lbl_usersPoints.text = ("\("Points: ")\(points)")
                    UserDefaults.standard.set(points, forKey: "userPoints")
                }
                else
                {
                    refo.child("users").child(userID).updateChildValues(["Points":0]) // added points to the profile
                    self.lbl_usersPoints.text = "Points: 0"
                    UserDefaults.standard.set(0, forKey: "userPoints")
                }
               
                
            }else
            {
               
            }
    //        self.loadingIndicator.stopAnimating()
    //        self.loadingIndicator.hidesWhenStopped = true
            
        }) { (error) in
            print(error.localizedDescription)
    //        self.loadingIndicator.stopAnimating()
     //       self.loadingIndicator.hidesWhenStopped = true
            
        }
        
    }

    func addToken()
    {
        //fcmToken added here
        if UserDefaults.standard.object(forKey: "fcmToken") as? String != nil
        {
            self.userID = (Auth.auth().currentUser?.uid)!
            let token = UserDefaults.standard.object(forKey: "fcmToken") as! String
           // print(token)
            Database.database().reference().child("users").child(self.userID).updateChildValues(["DeviceToken": token])
            UserDefaults.standard.removeObject(forKey: "fcmToken")
        }

    }
    
}
