//
//  ViewController.swift
//  FanClub
//
//  Created by Brst on 5/16/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore

class ViewController: UIViewController, FBSDKLoginButtonDelegate {
    var loadingIndicator = UIActivityIndicatorView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(FBSDKAccessToken.current())
    }
//MARK: View Will Appear Function
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true;
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func facebook_login(_ sender: Any)
    {
        loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        loadingIndicator.center = self.view.center
        loadingIndicator.color = UIColor.black
        self.loadingIndicator.hidesWhenStopped = true
        self.loadingIndicator.startAnimating()
        self.view.addSubview(self.loadingIndicator)

       // let loginManager = FBSDKLoginManager()
       // loginManager.logOut()
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile","user_friends","user_photos","user_location","user_education_history","user_birthday","user_posts"], from: self) { (result, error) in
            
            print(result!)
            print(FBSDKAccessToken.current())
            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                self.loadingIndicator.stopAnimating()
                return
            }
            guard let accessToken = FBSDKAccessToken.current() else {
                self.loadingIndicator.stopAnimating()
                print("Failed to get access token")
                return
            }
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            // Perform login by calling Firebase APIs
            Auth.auth().signIn(with: credential, completion: { (user, error) in
                if let error = error {
                    self.loadingIndicator.stopAnimating()
                    print("Login error: \(error.localizedDescription)")
                    let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
                    let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(okayAction)
                    self.present(alertController, animated: true, completion: nil)
                    return
                } else {
                    print(user?.email as Any)
                    if user?.email == nil
                    {
                        let alertController = UIAlertController(title: "Alert", message: "Please use email ID in facebook while login.", preferredStyle: .alert)
                        let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alertController.addAction(okayAction)
                        self.present(alertController, animated: true, completion: nil)

                    }
                    else
                    {
                        self.getFBUserData()
                        self.returnUserData()
                        
                     /*   let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let walkthroughVC = storyboard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
                        
                        let navigationController = self.window?.rootViewController as! UINavigationController
                        
                        navigationController.pushViewController(walkthroughVC, animated: false)*/

                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
                        
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                }
                self.loadingIndicator.stopAnimating()
            })
        }
    }

//MARK: Facebook Delegate Methods
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        print("User Logged In")
        
        if ((error) != nil)
        {
            print("Error =\(error)")
        }
        else if result.isCancelled {
        }
        else {
            if result.grantedPermissions.contains("email")
            {
                }
            
            self.returnUserData()
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    
    func getFBUserData(){
            if((FBSDKAccessToken.current()) != nil){
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, first_name, last_name, picture.type(large)"]).start(completionHandler: { (connection, result, error) -> Void in
                    if (error == nil){
                        let postDict = result as! [String : AnyObject]
                        print("postDict\(String(describing: postDict))")

                        let firstName = postDict["first_name"]
                        let lastName = postDict["last_name"]
                        let email = postDict["email"]
                        let profileImg = postDict["picture"]    as! [String : AnyObject]
                        let data = profileImg["data"]    as! [String : AnyObject]
                        let url = data["url"] as? String
                        print("url = \(String(describing: url!))")

                        //MARK: Saving Facebook Public information to user's profile :-
                        let ref = Database.database().reference()
                        let userID = Auth.auth().currentUser?.uid
                        UserDefaults.standard.set(userID, forKey: "userID")
                        ref.child("users").child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                            if snapshot.exists()
                            {
                                print("Current User exist")
                            }else
                                {
                                    print("New User")
                                    let userID = Auth.auth().currentUser?.uid
                                    print("userID\(String(describing: userID))")
                                    ref.child("users").child(userID!).updateChildValues(["First Name": firstName!, "Last Name": lastName!, "Email": email!, "Phone": "","Profile Image": url!,  "Gender": "", "Bio": "", ])
                            }
                        })
                    }
                })
        }
    }

    func returnUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                print("Error: \(String(describing: error))")
            }
            else
            {
                print("fetched user: \(result!)")
            }
        })
    }
    
    
    @IBAction func login_method(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "logIn") as! LoginViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    @IBAction func createAccount(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "register") as! RegisterViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }

}
