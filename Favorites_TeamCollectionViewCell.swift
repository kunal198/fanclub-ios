//
//  Favorites_TeamCollectionViewCell.swift
//  FanClub
//
//  Created by Brst on 5/19/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class Favorites_TeamCollectionViewCell: UICollectionViewCell {
    @IBOutlet var img: UIImageView!
    @IBOutlet var lbl: UILabel!
}
