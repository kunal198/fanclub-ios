//
//  CollectionViewCell.swift
//  FanClub
//
//  Created by Brst on 5/16/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbl: UILabel!
}
