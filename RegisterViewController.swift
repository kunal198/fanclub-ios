//
//  RegisterViewController.swift
//  FanClub
//
//  Created by Brst on 5/16/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore

class RegisterViewController: UIViewController , UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate {
    @IBOutlet var txt_firstName: UITextField!
    @IBOutlet var txt_lastName: UITextField!
    @IBOutlet var txt_email: UITextField!
    @IBOutlet var txt_password: UITextField!
    @IBOutlet var txt_phone: UITextField!
    @IBOutlet weak var view_firstname_register: UIView!
    @IBOutlet weak var view_lastname_register: UIView!
    @IBOutlet weak var view_email_register: UIView!
    @IBOutlet weak var view_phone_register: UIView!
    @IBOutlet var view_password: UIView!
    @IBOutlet var profile_img: UIImageView!
    var imagePicker = UIImagePickerController()
    var first = Bool()
    var last = Bool()
    var email = Bool()
    var password = Bool()
    var imgData = NSData()
    var loadingIndicator = UIActivityIndicatorView()
    var randomStr = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true;
        randomStr = randomString(length: 62)
        
        view_firstname_register.layer.cornerRadius = 5;
        view_firstname_register.layer.masksToBounds = true
        view_firstname_register.layer.borderWidth = 1.0
        view_firstname_register.layer.borderColor = UIColor.lightGray.cgColor
        view_lastname_register.layer.cornerRadius = 5;
        view_lastname_register.layer.masksToBounds = true
        view_lastname_register.layer.borderWidth = 1.0
        view_lastname_register.layer.borderColor = UIColor.lightGray.cgColor
        
        view_email_register.layer.cornerRadius = 5;
        view_email_register.layer.masksToBounds = true
        view_email_register.layer.borderWidth = 1.0
        view_email_register.layer.borderColor = UIColor.lightGray.cgColor
        view_phone_register.layer.cornerRadius = 5;
        view_phone_register.layer.masksToBounds = true
        view_phone_register.layer.borderWidth = 1.0
        view_phone_register.layer.borderColor = UIColor.lightGray.cgColor

        view_password.layer.cornerRadius = 5;
        view_password.layer.masksToBounds = true
        view_password.layer.borderWidth = 1.0
        view_password.layer.borderColor = UIColor.lightGray.cgColor
        
        profile_img.layer.cornerRadius =  self.profile_img.frame.width/2
        profile_img.layer.masksToBounds = true
        profile_img.layer.borderWidth = 1.0
        profile_img.layer.borderColor = UIColor.lightGray.cgColor
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
//MARK: Add Profile Image
    @IBAction func addProfileImage(_ sender: Any)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self .present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
                let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
                profile_img.contentMode = .scaleToFill
                profile_img.image = chosenImage
                profile_img.layer.masksToBounds = true
                dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
// MARK : text field functions
    private func textFieldDidEndEditing(_ textField: UITextField)-> Bool {
        
        return true;
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.view.frame.origin.y = 0;
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder();
        return true;
    }
    //MARK: Registration Done Here
    @IBAction func register_data(_ sender: Any)
    {
        loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        loadingIndicator.center = self.view.center
        loadingIndicator.color = UIColor.black
        loadingIndicator.startAnimating()
        self.loadingIndicator.hidesWhenStopped = true
        
        if txt_firstName.text != "" && txt_lastName.text != "" && txt_email.text != "" && txt_password.text != ""
        {
            
            let emailID = txt_email.text!
            let password1 = txt_password.text!
            let ref = Database.database().reference()
            Auth.auth().createUser(withEmail: emailID, password: password1) { (user, error) in
                if error == nil
                {
                    self.loadingIndicator.startAnimating()
                    self.view.addSubview(self.loadingIndicator)

                    let userID = Auth.auth().currentUser?.uid
                    print("UserID : = \(String(describing: userID!))")
                    if self.profile_img.image != UIImage(named: "man_2")
                    {
                    
                        self.uploadMedia() { url in
                            if url != nil
                            {
                                ref.child("users").child(userID!).updateChildValues(["First Name": self.txt_firstName.text!, "Last Name": self.txt_lastName.text!, "Email": self.txt_email.text!, "Phone": self.txt_phone.text!, "Profile Image": url!, "Gender": "", "Bio": ""])
                                print("You have successfully signed up")
                                print("user = \(String(describing: user!))")
                                let alert:UIAlertController = UIAlertController(title: "Alert", message: "Registered Successfully", preferredStyle: UIAlertControllerStyle.alert)
                                let loginAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.default)
                                {
                                    UIAlertAction in
                                   
                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                }
                                alert.addAction(loginAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    else    {
                        ref.child("users").child(userID!).updateChildValues(["First Name": self.txt_firstName.text!, "Last Name": self.txt_lastName.text!, "Email": self.txt_email.text!, "Phone": self.txt_phone.text!, "Profile Image": "", "Gender": "", "Bio": ""])
                        print("You have successfully signed up")
                        print("user = \(String(describing: user!))")
                        let alert:UIAlertController = UIAlertController(title: "Alert", message: "Registered Successfully", preferredStyle: UIAlertControllerStyle.alert)
                        let loginAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.default)
                        {
                            UIAlertAction in
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                        alert.addAction(loginAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    self.loadingIndicator.stopAnimating()
                } else {
                    print(error as Any)
                    self.loadingIndicator.stopAnimating()
                    let alert = UIAlertController(title: "Alert", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }

        }

        if txt_firstName.text == ""
        {
            let alert = UIAlertController(title: "Alert", message: "Enter First Name", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        if txt_lastName.text == ""
        {
            let alert = UIAlertController(title: "Alert", message: "Enter Last Name", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        if txt_email.text == ""
        {
            let alert = UIAlertController(title: "Alert", message: "Enter Email Name", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        if txt_password.text == ""
        {
            let alert = UIAlertController(title: "Alert", message: "Enter Password Name", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }

//MARK: upload image to database :-
    func uploadMedia(completion: @escaping (_ url: String?) -> Void) {
        let storageRef = Storage.storage().reference().child(randomStr)
        if let uploadData = UIImageJPEGRepresentation(self.profile_img.image!, 0) {
            storageRef.putData(uploadData, metadata: nil) { (metadata, error) in
                if error != nil {
                    print("error")
                    completion(nil)
                } else {
                    completion((metadata?.downloadURL()?.absoluteString)!)
                    // your uploaded photo url.
                }
            }
        }
    }
   
    @IBAction func back_btn(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
//MARK: Random String
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
//            randomStr = randomString
//            print("Random String for Image = \(randomStr)")
        }
        
        return randomString
    }
  
}
