//
//  Edit_ProfileViewController.swift
//  FanClub
//
//  Created by Brst on 5/17/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore
import SDWebImage

class Edit_ProfileViewController: UIViewController , UITextFieldDelegate , UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet var txt_email: UITextField!
    @IBOutlet var txt_firstName: UITextField!
    @IBOutlet var txt_lastName: UITextField!
    @IBOutlet var txt_phone: UITextField!
    @IBOutlet var txt_bio: UITextView!
    @IBOutlet var img_profile: UIImageView!
    @IBOutlet var profile_img: UIButton!
    
    @IBOutlet weak var bt_fanBoy: UIButton!
    @IBOutlet weak var bt_fanGirl: UIButton!
    @IBOutlet weak var lbl_fanBoy: UILabel!
    @IBOutlet weak var lbl_fanGirl: UILabel!
    @IBOutlet weak var view_textview: UIView!
    @IBOutlet weak var view_phone: UIView!
    @IBOutlet weak var view_lastname: UIView!
    @IBOutlet weak var view_firstname: UIView!
    
    var check = Bool()
    var check1 = Bool()
    var dict = NSMutableArray()
    var genderStr = String()
    var imagePicker = UIImagePickerController()
    var imgData = NSData()
    var loadingIndicator = UIActivityIndicatorView()
    var randomStr = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true;
        randomStr = randomString(length: 62)

        view_firstname.layer.cornerRadius = 5;
        view_firstname.layer.masksToBounds = true
        view_firstname.layer.borderWidth = 1.0
        view_firstname.layer.borderColor = UIColor.lightGray.cgColor
        
        view_lastname.layer.cornerRadius = 5;
        view_lastname.layer.masksToBounds = true
        view_lastname.layer.borderWidth = 1.0
        view_lastname.layer.borderColor = UIColor.lightGray.cgColor

        view_phone.layer.cornerRadius = 5;
        view_phone.layer.masksToBounds = true
        view_phone.layer.borderWidth = 1.0
        view_phone.layer.borderColor = UIColor.lightGray.cgColor
        
        view_textview.layer.cornerRadius = 5;
        view_textview.layer.masksToBounds = true
        view_textview.layer.borderWidth = 1.0
        view_textview.layer.borderColor = UIColor.lightGray.cgColor
        
        img_profile.layer.cornerRadius =  self.img_profile.frame.width/2
        img_profile.layer.masksToBounds = true
        img_profile.layer.borderWidth = 1.0
        img_profile.layer.borderColor = UIColor.lightGray.cgColor
        

//MARK: Fetching data from Database :
        let ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            print("Snapshot = \(snapshot)")
          //  self.tabBarController?.tabBar.isHidden = true;

            let postDict = snapshot.value as! [String : AnyObject]
            print("postDict = \(postDict)")

            let firstName = postDict["First Name"]
            let lastName = postDict["Last Name"]
            let phone = postDict["Phone"]
            let email = postDict["Email"]
            let profileImg = postDict["Profile Image"]
            let gender = postDict["Gender"] as? String
            let bio = postDict["Bio"]
            
            self.txt_email.text = email as? String
            self.txt_firstName.text = firstName as? String
            self.txt_lastName.text = lastName as? String
            self.txt_bio.text = bio as? String
            self.txt_phone.text = phone as? String
            if gender == "girl"
            {
                self.genderStr = "girl"
                self.bt_fanGirl .setImage(UIImage(named: "star"), for: UIControlState.normal)
                self.bt_fanBoy .setImage(UIImage(named: "white_star"), for: UIControlState.normal)
            }
            else
            {
                self.genderStr = "boy"
                self.bt_fanBoy .setImage(UIImage(named: "star"), for: UIControlState.normal)
                self.bt_fanGirl .setImage(UIImage(named: "white_star"), for: UIControlState.normal)
            }
            self.img_profile.sd_setShowActivityIndicatorView(true)
            self.img_profile.sd_setIndicatorStyle(.gray)
            self.img_profile.sd_setImage(with: URL(string: profileImg as! String), placeholderImage: UIImage(named: "man_2"))
            
        }) { (error) in
            print(error.localizedDescription)
            }
}
//MARK: View Will Appear Function 
 override func viewWillAppear(_ animated: Bool)
 {
     super.viewWillAppear(animated)
    self.tabBarController?.tabBar.isHidden = true;

}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func selected_fanGirl_button(_ sender: UIButton)
    {
            bt_fanGirl .setImage(UIImage(named: "star"), for: UIControlState.normal)
            bt_fanBoy .setImage(UIImage(named: "white_star"), for: UIControlState.normal)
            self.genderStr = "girl"

    }
    @IBAction func selected_fanBoy_button(_ sender: UIButton)
    {
            bt_fanBoy .setImage(UIImage(named: "star"), for: UIControlState.normal)
            bt_fanGirl .setImage(UIImage(named: "white_star"), for: UIControlState.normal)
            self.genderStr = "male"
    }

// MARK : text field functions
    private func textFieldDidEndEditing(_ textField: UITextField)-> Bool {
        
        return true;
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.view.frame.origin.y = 0;
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    @IBAction func saveProfile(_ sender: Any)
    {
        loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        loadingIndicator.center = self.view.center
        loadingIndicator.color = UIColor.black
        loadingIndicator.startAnimating()
        self.loadingIndicator.hidesWhenStopped = true
        
        loadingIndicator.startAnimating()
        self.view.addSubview(loadingIndicator)

        let ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        self.uploadMedia() { url in
            if url != nil
            {
                ref.child("users").child(userID!).updateChildValues(["First Name": self.txt_firstName.text!, "Last Name": self.txt_lastName.text!, "Email": self.txt_email.text!, "Phone": self.txt_phone.text!, "Profile Image": url!, "Gender": self.genderStr, "Bio": self.txt_bio.text])
            }
            else
            {
                ref.child("users").child(userID!).updateChildValues(["First Name": self.txt_firstName.text!, "Last Name": self.txt_lastName.text!, "Email": self.txt_email.text!, "Phone": self.txt_phone.text!,  "Gender": self.genderStr, "Bio": self.txt_bio.text])
            }
            let name = "\(String(describing: self.txt_firstName.text!)) \(String(describing: self.txt_lastName.text!))"
            UserDefaults.standard.set(name, forKey: "name")
            UserDefaults.standard.set(self.txt_email.text!, forKey: "email")
            UserDefaults.standard.set(self.genderStr, forKey: "gender")
            UserDefaults.standard.set(self.txt_bio.text, forKey: "bio")
            UserDefaults.standard.set(self.txt_phone.text!, forKey: "phone")
            UserDefaults.standard.set(self.txt_firstName.text!, forKey: "firstname")
            UserDefaults.standard.set(self.txt_lastName.text!, forKey: "lastname")

        self.loadingIndicator.stopAnimating()
        let alert:UIAlertController = UIAlertController(title: "Alert", message: "Profile Updated Successfully", preferredStyle: UIAlertControllerStyle.alert)
        let loginAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        alert.addAction(loginAction)
        self.present(alert, animated: true, completion: nil)
        }
    }

    @IBAction func addProfileImage(_ sender: Any)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }

    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self .present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        img_profile.contentMode = .scaleToFill
        img_profile.image = chosenImage
        img_profile.layer.masksToBounds = true
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
//MARK: upload image to database :-
    func uploadMedia(completion: @escaping (_ url: String?) -> Void) {
        let storageRef = Storage.storage().reference().child(randomStr)
        if let uploadData = UIImageJPEGRepresentation(self.img_profile.image!, 0) {
            storageRef.putData(uploadData, metadata: nil) { (metadata, error) in
                if error != nil {
                    print("error")
                    completion(nil)
                } else {
                    completion((metadata?.downloadURL()?.absoluteString)!)
                    // your uploaded photo url.
                }
            }
        }
    }
    @IBAction func back_btn(_ sender: Any)
    {
        _ = navigationController?.popToRootViewController(animated: true)

//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
//        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    //MARK: Random String
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
 }
