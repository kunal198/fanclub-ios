//
//  Teams_ViewController.swift
//  FanClub
//
//  Created by Brst on 5/18/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore
import SDWebImage
import MapKit

class Teams_ViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource , UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate{

    var cell : Teams_CollectionViewCell!
    var cell1 : OtherFans_CollectionViewCell!
    
    @IBOutlet var indicator_teams: UIActivityIndicatorView!
    @IBOutlet var indicator_users: UIActivityIndicatorView!
    @IBOutlet var indicator_events: UIActivityIndicatorView!
    @IBOutlet var lbl_team: UILabel!
    @IBOutlet var lbl_fan: UILabel!
    @IBOutlet var lbl_events: UILabel!
    @IBOutlet var btn_teams: UIButton!
    @IBOutlet var btn_otherFans: UIButton!
    @IBOutlet var btn_teamEvents: UIButton!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var collectionView1: UICollectionView!
    @IBOutlet var collectionView2: UICollectionView!
    
    
    
    @IBOutlet var view_profileShowing: UIView!
    @IBOutlet var txt_email: UITextField!
    @IBOutlet var txt_firstName: UITextField!
    @IBOutlet var txt_lastName: UITextField!
    @IBOutlet var txt_phone: UITextField!
    @IBOutlet var txt_bio: UITextView!
    @IBOutlet var img_profile: UIImageView!
    
    @IBOutlet weak var bt_fanGirl: UIButton!
    @IBOutlet weak var lbl_fanGirl: UILabel!
    @IBOutlet weak var view_textview: UIView!
    @IBOutlet weak var view_phone: UIView!
    @IBOutlet weak var view_lastname: UIView!
    @IBOutlet weak var view_firstname: UIView!
    @IBOutlet var indicator_profile: UIActivityIndicatorView!
    
    
    
    
    var arrayImg = NSMutableArray()
    var data = Int()
    var arrayName = NSMutableArray()
    var arrayNCCA = NSMutableArray()
    var getNFL = NSMutableArray()
    var lbl = UILabel()
    var indexPath_selected = Int()
    var UsersID = NSMutableArray()
    var str = String()
    var selectedTeam = String()
    var arrayFnl = NSMutableArray()
    var demo = String()
    
    var arrayLocation = NSMutableArray()
    var arrayTitle = NSMutableArray()
    var arrayTime = NSMutableArray()
    var arrayTeam = NSMutableArray()
    var getUserID = NSMutableArray()
    var arrayKey = NSMutableArray()
   
    var arrayLatitude = NSMutableArray()
    var arrayLongitude = NSMutableArray()
    
    var strLongitude = Double()
    var strLatitude = Double()
    let locationManager = CLLocationManager()
    var startLocation: CLLocation!
    var lastLocation: CLLocation!
    var startDate: Date!
    var traveledDistance: Double = 0
    
    
    var check = Bool()
    var check1 = Bool()
    var dict = NSMutableArray()
    var genderStr = String()
    var imagePicker = UIImagePickerController()
    var imgData = NSData()
    var loadingIndicator = UIActivityIndicatorView()
    var randomStr = String()
    var userID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        btn_teams.layer.cornerRadius = self.btn_teams.frame.size.height/2
        btn_teams.layer.masksToBounds = true
        btn_otherFans.layer.cornerRadius = self.btn_otherFans.frame.size.height/2
        btn_otherFans.layer.masksToBounds = true
        btn_teamEvents.layer.cornerRadius = self.btn_teamEvents.frame.size.height/2
        btn_teamEvents.layer.masksToBounds = true
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.distanceFilter = 10
            
        }

        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations.last ?? "none")
        
        if startDate == nil {
            startDate = Date()
        } else {
        }
        
        if startLocation == nil {
            startLocation = locations.first
        } else if let location = locations.last {
           
            
            strLatitude = location.coordinate.latitude
            strLongitude = location.coordinate.longitude
            
        }
        lastLocation = locations.last
    }

    
    
//MARK: $$$ View Will Appear :-
    override func viewWillAppear(_ animated: Bool)
    {
        self.arrayLocation.removeAllObjects()
        self.arrayTime.removeAllObjects()
        self.arrayTitle.removeAllObjects()
        self.arrayTeam.removeAllObjects()
        self.arrayKey.removeAllObjects()
        self.getUserID.removeAllObjects()
        self.arrayFnl.removeAllObjects()
        self.tableView.reloadData()

        self.UsersID.removeAllObjects()
        self.UsersID.removeAllObjects()
        self.arrayName.removeAllObjects()
        self.arrayImg.removeAllObjects()
        self.collectionView2.reloadData()
        
        UserDefaults.standard.removeObject(forKey: "dataDef1")
        self.indicator_teams.startAnimating()
        self.arrayName.removeAllObjects()
        self.arrayImg.removeAllObjects()
        self.tableView.reloadData()
        self.collectionView2.reloadData()
        self.collectionView1.reloadData()
        self.indicator_teams.center = self.collectionView1.center
        self.lbl_team.center = self.collectionView1.center
        self.lbl_events.center = self.tableView.center
        self.indicator_events.center = self.tableView.center
        self.lbl_fan.center = self.collectionView2.center
        self.indicator_users.center = self.collectionView2.center
        
        self.gettingTeams()
    }
    
//MARK: @@@ Getting Teams
    func gettingTeams()
    {
        self.getNFL.removeAllObjects()
        let ref = Database.database().reference()
        ref.child("Sports").child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            if snapshot.exists()
            {
                print(snapshot.value!)
                self.collectionView1.isHidden = false
                self.lbl_team.isHidden = true
                
                var postDict = snapshot.value as! [String : AnyObject]
                
                let array = postDict["NFL"] as? NSMutableArray
                if array == nil
                {
                    
                }
                else
                {
                    self.getNFL.addObjects(from: array as! [Any])
                    
                }
                
                let array1 = postDict["NBA"] as? NSMutableArray
                if array1 == nil
                {
                    
                }
                else
                {
                    self.getNFL.addObjects(from: array1 as! [Any])
                    
                }
                
                let array2 = postDict["MLB"] as? NSMutableArray
                if array2 == nil
                {
                    
                }
                else
                {
                    self.getNFL.addObjects(from: array2 as! [Any])
                }
                
                let array3 = postDict["NCCA_Football"] as? NSMutableArray
                if array3 == nil
                {
                    
                }
                else
                {
                    self.getNFL.addObjects(from: array3 as! [Any])
                }
                self.indicator_teams.stopAnimating()
                self.selectedTeam = self.getNFL[0] as! String
                UserDefaults.standard.set(0, forKey: "dataDef1")
             
                self.indicator_users.startAnimating()
                self.gettingFans()
                self.collectionView1.reloadData()
                
            }
            else
            {
                self.indicator_teams.stopAnimating()
                self.collectionView1.isHidden = true
                self.lbl_team.isHidden = false
                self.lbl_fan.isHidden = false
                self.lbl_events.isHidden = false
                self.getNFL.removeAllObjects()
                self.collectionView1.reloadData()
            }
            
        })
    }
    //MARK: Getting Users:-
    func gettingFans()
    {
      //  self.indicator_users.startAnimating()
        self.indicator_events.startAnimating()
        self.UsersID.removeAllObjects()
        self.UsersID.removeAllObjects()
        self.arrayName.removeAllObjects()
        self.arrayImg.removeAllObjects()
        self.collectionView2.reloadData()
        
        if UserDefaults.standard.object(forKey: "dataDef1") != nil
        {
            let indexPath = UserDefaults.standard.object(forKey: "dataDef1") as! Int
            selectedTeam = self.getNFL[indexPath] as! String
            let ref = Database.database().reference()
            let user = Auth.auth().currentUser?.uid
            ref.child("Favourites").child(selectedTeam).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                if snapshot.exists()
                {
                    self.lbl_fan.isHidden = true
                    self.lbl_events.isHidden = true
                    for a in ((snapshot.value as AnyObject).allKeys)!
                    {
                        self.collectionView1.isUserInteractionEnabled = false
                        self.str = a as! String
                        if self.str == user
                        {
                            print(user!,a)
                        }
                        else
                        {
                            self.indicator_users.startAnimating()
                            self.indicator_events.startAnimating()
                            self.lbl_fan.isHidden = true
                            self.lbl_events.isHidden = true

                            self.collectionView1.isUserInteractionEnabled = false
                            self.UsersID.add(a)
                            print(self.UsersID)
                            let ref1 = Database.database().reference()
                            ref1.child("users").child(self.str).observeSingleEvent(of: .value, with: { (snapshot) in
                                if snapshot.exists()
                                {
                                    print(snapshot)
                                    var postDict = snapshot.value as! [String : AnyObject]
                                    let array = postDict["First Name"]
                                    self.arrayName.add(array!)
                                    let images = postDict["Profile Image"]
                                    self.arrayImg.add(images!)
                                    self.indicator_users.stopAnimating()
                                    self.indicator_users.isHidden = true
                                    self.collectionView1.isUserInteractionEnabled = true
                                    self.collectionView2.reloadData()
                                   
                                }
                                else
                                {
                                    self.indicator_users.stopAnimating()
                                    self.collectionView1.isUserInteractionEnabled = true

                                }
                                
                            })
                        }
                        
                    }
                    if self.UsersID.count == 0
                    {
                        self.indicator_users.stopAnimating()
                        self.indicator_events.stopAnimating()
                        self.lbl_fan.isHidden = false
                        self.lbl_events.isHidden = false
                        self.tableView.reloadData()
                        self.collectionView1.isUserInteractionEnabled = true
                        
                    }
                }
                else
                {
                    self.collectionView1.isUserInteractionEnabled = true
                    self.indicator_users.stopAnimating()
                    self.lbl_fan.isHidden = false
                }
            })
           
        }
        self.indicator_users.stopAnimating()
        self.gettingEvents()
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: Collection View Functions :-
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionView1    {
            return getNFL.count   }
        else    {
            return arrayName.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionView1   {
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Teams_CollectionViewCell
        cell.layer.cornerRadius = 5;
        cell.layer.masksToBounds = true
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.lbl.text = getNFL[indexPath.row] as? String
        if  UserDefaults.standard.object(forKey: "dataDef1") != nil
        {
            if indexPath.row == UserDefaults.standard.object(forKey: "dataDef1")as! Int
            
                {
                    cell.layer.cornerRadius = 5;
                    cell.layer.masksToBounds = true
                    cell.layer.borderWidth = 1.5
                    cell.layer.borderColor = UIColor.red.cgColor
                }
            }
        return cell
        }
       if collectionView == collectionView2 {
            cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! OtherFans_CollectionViewCell
            cell1.layer.cornerRadius = 5;
            cell1.layer.masksToBounds = true
            cell1.layer.borderWidth = 1.0
            cell1.layer.borderColor = UIColor.lightGray.cgColor
            cell1.lbl.text = arrayName[indexPath.row] as? String
        
            let profileImg = arrayImg[indexPath.row] as? String
            cell1.img.sd_setShowActivityIndicatorView(true)
            cell1.img.sd_setIndicatorStyle(.gray)
            cell1.img.sd_setImage(with: URL(string: profileImg!), placeholderImage: UIImage(named: "man_2"))

            return cell1
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionView1
        {
            indexPath_selected = indexPath.row
            UserDefaults.standard.set(indexPath.row, forKey: "dataDef1")
            collectionView.reloadData()
            self.collectionView1.isUserInteractionEnabled = false
            self.arrayKey.removeAllObjects()
            self.arrayLocation.removeAllObjects()
            self.arrayTime.removeAllObjects()
            self.arrayTitle.removeAllObjects()
            self.arrayTeam.removeAllObjects()
            self.arrayFnl.removeAllObjects()
            self.getUserID.removeAllObjects()
            self.getUserID.removeAllObjects()
            tableView.reloadData()
            self.indicator_users.startAnimating()
            self.gettingFans()
            
        }
        else
        {
            
            self.indicator_profile.isHidden = false
            userID = self.UsersID[indexPath.row] as! String
            UserDefaults.standard.set(userID, forKey: "userId")
           // self.view_profileShowing.isHidden = false
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "profile_viewController") as! ProfileViewController
            nextViewController.checkprofile = true
            self.navigationController?.pushViewController(nextViewController, animated: true)
     /*       UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
                self.view_profileShowing.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    self.view_profileShowing.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                        self.view_profileShowing.transform = CGAffineTransform.identity
                    })
                })
            })
            */
          //  self.showingProfile()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTitle.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        cell.frame.size.width = self.view.frame.size.width
        cell.view_cell.layer.shadowColor = UIColor.black.cgColor
        cell.view_cell.layer.shadowOpacity = 1
        cell.view_cell.layer.shadowOffset = CGSize.zero
        cell.view_cell.layer.shadowRadius = 10
        cell.view_cell.layer.cornerRadius = 5
        cell.view_cell.layer.borderColor = UIColor.white.cgColor
        cell.view_cell.layer.borderWidth = 0.5
        cell.view_cell.clipsToBounds = true
        //cell.view_cell.layer.shadowPath = UIBezierPath(rect: cell.view_cell.bounds).cgPath
        cell.view_cell.layer.shouldRasterize = true
        cell.img_icon.layer.cornerRadius = 5;
        cell.img_icon.layer.masksToBounds = true
        cell.img_icon.layer.borderWidth = 1.0
        cell.img_icon.layer.borderColor = UIColor.lightGray.cgColor
        cell.lbl_location.text = arrayLocation[indexPath.row] as? String
        cell.lbl_calender.text = arrayTime[indexPath.row] as? String
        cell.lbl_eventName.text = arrayTitle[indexPath.row] as? String
        cell.lbl_icon_img.text = arrayTeam[indexPath.row] as? String
        
        let myLocation = CLLocation(latitude: strLatitude, longitude: strLongitude)
        
        let myBuddysLocation = CLLocation(latitude: arrayLatitude[indexPath.row] as! CLLocationDegrees, longitude: arrayLongitude[indexPath.row] as! CLLocationDegrees)
        
        let distance = myLocation.distance(from: myBuddysLocation) / 1609
        
        
        cell.lbl_distance.text = String(format: "%.01f Miles", distance)
        

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 99
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        if #available(iOS 9.0, *) {
           
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "create_watchParty") as! Create_WatchParty_ViewController
                nextViewController.checkParty = true
                nextViewController.strKey = arrayKey[indexPath.row] as! String
                nextViewController.strTeam = arrayTeam[indexPath.row] as! String
                nextViewController.strTitle = arrayTitle[indexPath.row] as! String
                nextViewController.strTime = arrayTime[indexPath.row] as! String
                nextViewController.strLocation = arrayLocation[indexPath.row] as! String
                
                self.navigationController?.pushViewController(nextViewController, animated: true)
        } else {
            // Fallback on earlier versions
        }
    }
    
    //MARK:$$$ Invited Parties :-
    func gettingEvents()
    {
        self.indicator_events.startAnimating()
        self.arrayKey.removeAllObjects()
        self.arrayLocation.removeAllObjects()
        self.arrayTime.removeAllObjects()
        self.arrayTitle.removeAllObjects()
        self.arrayTeam.removeAllObjects()
        self.arrayFnl.removeAllObjects()
        self.getUserID.removeAllObjects()
        self.getUserID.removeAllObjects()
        tableView.reloadData()
        // getting keys here
        let ref = Database.database().reference()
        ref.child("users").child((Auth.auth().currentUser?.uid)!).child("Party").child("Invited").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists()
            {
                self.lbl_events.isHidden = true
                for a in ((snapshot.value as AnyObject).allKeys)!
                {
                    if self.getUserID.contains(a)
                    {
                        self.lbl_events.isHidden = false

                    }
                    else
                    {
                        self.lbl_events.isHidden = true
                        self.getUserID.add(a)
                    }
                }
            }
            else
            {
                self.indicator_events.stopAnimating()
                self.lbl_events.isHidden = false
                
                self.arrayLocation.removeAllObjects()
                self.arrayTime.removeAllObjects()
                self.arrayTitle.removeAllObjects()
                self.arrayTeam.removeAllObjects()
                self.arrayKey.removeAllObjects()
                self.getUserID.removeAllObjects()
                self.arrayFnl.removeAllObjects()
                self.tableView.reloadData()
            }
            self.getUsersCount()
            
        })
        
    }
    // Counting users for Invited Parties
    func getUsersCount()
    {
        if getUserID.count != 0
        {
            for i in 0..<getUserID.count
            {
                // getting data here for keys
                let ref1 = Database.database().reference()
                ref1.child("users").child((Auth.auth().currentUser?.uid)!).child("Party").child("Invited").child(getUserID[i] as! String).observeSingleEvent(of: .value, with: { (snapshot) in
                    if snapshot.exists()
                    {
                        let postDict1 = snapshot.value as! [String : AnyObject]
                        let data = postDict1["Team"]
                        print(data!)
                        if (data?.isEqual(self.selectedTeam))!
                        {
                            self.lbl_events.isHidden = true
                            self.demo = self.getUserID[i] as! String
                            self.arrayFnl.addObjects(from: [self.getUserID[i]])
                            self.gettingData()
                        }
                        else
                        {
                            self.indicator_events.stopAnimating()
                            self.lbl_events.isHidden = false
                        }
                       
                    }
                    else
                    {
                        self.lbl.isHidden = false
                        self.indicator_events.stopAnimating()
                    }
                })
            }
            //
        }
        else
        {
            self.indicator_events.stopAnimating()
          //  self.lbl.isHidden = false
            self.tableView.reloadData()
        }
    }
    
    // Counting users for Invited Parties
    func gettingData()
    {
//        if self.arrayFnl.count != 0
//        {
//            for i in 0..<arrayFnl.count
//            {
       
            
            let ref1 = Database.database().reference()
            ref1.child("Party").child(self.demo).observeSingleEvent(of: .value, with: { (snapshot) in  //child(self.arrayFnl[i] as! String)
                if snapshot.exists()
                {
                   // print(snapshot)
                    self.lbl_events.isHidden = true
                    let postDict1 = snapshot.value as! [String : AnyObject]
                    print(postDict1["Status"] as! String)
                    if postDict1["Status"] as! String == "Public"
                    {
                        // Adding child key
                        self.arrayKey.add(snapshot.key)
                        let array = postDict1["Location"]
                        self.arrayLocation.add(array!)
                        
                        let array2 = postDict1["Team_Name"]
                        self.arrayTeam.add(array2!)
                        
                        let array3 = postDict1["Title"]
                        self.arrayTitle.add(array3!)
                        
                        let array4 = postDict1["Time"]
                        self.arrayTime.add(array4!)
                       
                        let array5 = postDict1["location_laitude"]
                        self.arrayLatitude.add(array5!)
                        
                        let array6 = postDict1["location_longitude"]
                        self.arrayLongitude.add(array6!)

                        self.indicator_events.stopAnimating()
                        self.tableView.reloadData()
                    }
    
                }
                else
                {
                    self.lbl.isHidden = false
                    self.indicator_events.stopAnimating()

                }
                
            })
          //  }
       // }
//        else
//        {
//           // self.loadingIndicator1.stopAnimating()
//            self.tableView.reloadData()
//        }
        DispatchQueue.main.async {
            if self.arrayKey.count == 0
            {
                self.tableView.reloadData()
                self.lbl.isHidden = false
            }
            else
            {
                self.lbl.isHidden = true
            }
        }
    
    }
 //MARK:%%%%%%<<<<<<<<<   Showing profile here
    func showingProfile()
    {
        self.tabBarController?.tabBar.isHidden = true;
        
        view_firstname.layer.cornerRadius = 5;
        view_firstname.layer.masksToBounds = true
        view_firstname.layer.borderWidth = 1.0
        view_firstname.layer.borderColor = UIColor.lightGray.cgColor
        
        view_lastname.layer.cornerRadius = 5;
        view_lastname.layer.masksToBounds = true
        view_lastname.layer.borderWidth = 1.0
        view_lastname.layer.borderColor = UIColor.lightGray.cgColor
        
        view_phone.layer.cornerRadius = 5;
        view_phone.layer.masksToBounds = true
        view_phone.layer.borderWidth = 1.0
        view_phone.layer.borderColor = UIColor.lightGray.cgColor
        
        view_textview.layer.cornerRadius = 5;
        view_textview.layer.masksToBounds = true
        view_textview.layer.borderWidth = 1.0
        view_textview.layer.borderColor = UIColor.lightGray.cgColor
        
        img_profile.layer.cornerRadius =  self.img_profile.frame.width/2
        img_profile.layer.masksToBounds = true
        img_profile.layer.borderWidth = 1.0
        img_profile.layer.borderColor = UIColor.lightGray.cgColor
        
        //MARK: Fetching data from Database :
        let ref = Database.database().reference()
     //   let userID = FIRAuth.auth()?.currentUser?.uid
        ref.child("users").child(userID).observeSingleEvent(of: .value, with: { (snapshot) in
           // print("Snapshot = \(snapshot)")
            //  self.tabBarController?.tabBar.isHidden = true;
            
            let postDict = snapshot.value as! [String : AnyObject]
          //  print("postDict = \(postDict)")
            self.indicator_profile.isHidden = true
            
            
            let firstName = postDict["First Name"]
            let lastName = postDict["Last Name"]
            let phone = postDict["Phone"]
            let email = postDict["Email"]
            let profileImg = postDict["Profile Image"]
            let gender = postDict["Gender"] as? String
            let bio = postDict["Bio"]
            
            self.txt_email.text = email as? String
            self.txt_firstName.text = firstName as? String
            self.txt_lastName.text = lastName as? String
            self.txt_bio.text = bio as? String
            self.txt_phone.text = phone as? String
            if gender == "girl"
            {
              
                self.lbl_fanGirl.text = "Girl"
            }
            else
            {
           
                self.lbl_fanGirl.text = "Boy"

            }
            self.img_profile.sd_setShowActivityIndicatorView(true)
            self.img_profile.sd_setIndicatorStyle(.gray)
            self.img_profile.sd_setImage(with: URL(string: profileImg as! String), placeholderImage: UIImage(named: "man_2"))
            
        }) { (error) in
            print(error.localizedDescription)
            self.indicator_profile.isHidden = true
            
        }

    }
    
    
    @IBAction func btn_showingProfile_back(_ sender: Any)
    {
        self.txt_firstName.text = ""
        self.txt_lastName.text = ""
        self.txt_bio.text = ""
        self.txt_phone.text = ""
        self.img_profile.image = UIImage(named:"man_2" )
        self.txt_email.text = ""
        self.lbl_fanGirl.text = ""
        
        self.tabBarController?.tabBar.isHidden = false
        self.view_profileShowing.isHidden = true
    }
    
    
    
    
}








