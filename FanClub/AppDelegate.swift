

//  AppDelegate.swift
//  FanClub
//
//  Created by Brst on 5/16/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore
import FBSDKCoreKit
import FBSDKLoginKit
import SDWebImage
import UserNotifications
import FirebaseMessaging
import FirebaseInstanceID

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    
    static var shared: AppDelegate { return UIApplication.shared.delegate as! AppDelegate }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.sharedManager().enable = true
         UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.gray], for: .normal)
        FirebaseApp.configure()
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

        if(FBSDKAccessToken.current() != nil)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let walkthroughVC = storyboard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
            
            let navigationController = self.window?.rootViewController as! UINavigationController
            
            navigationController.pushViewController(walkthroughVC, animated: false)
        } else if (UserDefaults.standard.object(forKey: "userIdCheck") as? String) != nil//(Auth.auth().currentUser != nil)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let walkthroughVC = storyboard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
        
            let navigationController = self.window?.rootViewController as! UINavigationController
            
            navigationController.pushViewController(walkthroughVC, animated: false)
        }
        Auth.auth().addStateDidChangeListener { auth, user in
            if user != nil {
                // User is signed in.
            } else {
                // No user is signed in.
            }
        }
        
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
                         let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: NSNotification.Name.InstanceIDTokenRefresh,
                                               object: nil)
       
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        
        return handled
    }
    var applicationStateString: String {
        if UIApplication.shared.applicationState == .active {
            return "active"
        } else if UIApplication.shared.applicationState == .background {
            return "background"
        }else {
            return "inactive"
        }
    }
    
    func requestNotificationAuthorization(application: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
    }
//}
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()

    }

    func applicationDidEnterBackground(_ application: UIApplication) {
      
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
            }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0

        FBSDKAppEvents.activateApp()

         }

    func applicationWillTerminate(_ application: UIApplication) {
        NotificationCenter.default.removeObserver(self, name: .UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIApplicationWillEnterForeground, object: nil)

        UserDefaults.standard.removeObject(forKey: "checkProfile")
        UserDefaults.standard.removeObject(forKey: "dataDef")
      //  UserDefaults.standard.removeObject(forKey: "notiPartyKey")
    }
    
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    func tokenRefreshNotification(notification: NSNotification) {
        
        let refreshedToken = InstanceID.instanceID().token()
        print("InstanceID token: \(refreshedToken!)")
        
        let notificationName = Notification.Name("addToken")
        NotificationCenter.default.post(name: notificationName, object: nil)
        
        connectToFcm()
    }
    
    func connectToFcm() {
        Messaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
                // Messaging.messaging().subscribe(toTopic: "/topics/default-notifications-channel")
            }
        }
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
       
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1

        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
       
        if let messageID = userInfo[gcmMessageIDKey] {
           // print("Message ID: \(messageID)")
        }
        
   
        let currentCountStr = UIApplication.shared.applicationIconBadgeNumber.description
        let currentCount = Int(currentCountStr)
        if(currentCount! > 0) {
            UIApplication.shared.applicationIconBadgeNumber = currentCount! + 1
        } else {
            UIApplication.shared.applicationIconBadgeNumber = 1
        }

        completionHandler(UIBackgroundFetchResult.newData)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
   
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
       // InstanceID.instanceID().
        Messaging.messaging().apnsToken = deviceToken
        

        if let refreshedToken = InstanceID.instanceID().token() {
           // print("InstanceID token: \(refreshedToken)")
        }
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("++++++++++++++ token +++++++++++++++ = \(token)")
        
        
        InstanceID.instanceID().setAPNSToken(deviceToken, type: InstanceIDAPNSTokenType.sandbox)
       // InstanceID.instanceID().setAPNSToken(deviceToken, type: InstanceIDAPNSTokenType.prod)
        // With swizzling disabled you must set the APNs token here.
    }


}
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
   func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void) {
    
        let userInfo = notification.request.content.userInfo
    
        if let messageID = userInfo[gcmMessageIDKey] {
           // print("Message ID: \(messageID)")
        }
    let currentCountStr = UIApplication.shared.applicationIconBadgeNumber.description
    let currentCount = Int(currentCountStr)
    if(currentCount! > 0) {
        UIApplication.shared.applicationIconBadgeNumber = currentCount! + 1
    } else {
        UIApplication.shared.applicationIconBadgeNumber = 1
    }
    
    
        print(userInfo)
    
        completionHandler([.alert, .sound, .badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
    
        let dict = userInfo as NSDictionary
        print(dict)
        let partyKey = dict.value(forKey: "PartyKey") as! String
        let partyName = dict.value(forKey: "PartyName") as! String
        //        let partyKey = dict.value(forKey: "gcm.notification.PartyKey") as! String
        //        let partyName = dict.value(forKey: "gcm.notification.PartyName") as! String
        //  UserDefaults.standard.set(partyKey, forKey: "notiPartyKey")
        UserDefaults.standard.set(partyName, forKey: "partyName")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(partyKey, forKey: "partyKey")
        UserDefaults.standard.synchronize()
       
        UserDefaults.standard.set("making root controller", forKey: "notiPartyKey")
        UserDefaults.standard.set("msg", forKey: "chatScreenNotification1")
        print(userInfo)
        
        let notificationName = Notification.Name("chatScreenNotification")
        NotificationCenter.default.post(name: notificationName, object: nil)

        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let walkthroughVC = storyboard.instantiateViewController(withIdentifier: "chatScreen") as! ChatScreen_ViewController
        let navigationController = self.window?.rootViewController as! UINavigationController
        
        navigationController.pushViewController(walkthroughVC, animated: false)
    
        completionHandler()
    }
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
    }
  
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}
