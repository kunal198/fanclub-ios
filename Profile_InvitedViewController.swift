//
//  Profile_InvitedViewController.swift
//  FanClub
//
//  Created by Brst on 5/16/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore
import MapKit

class Profile_InvitedViewController: UIViewController , UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var lbl: UILabel!
    @IBOutlet var view1: UIView!
    
    var arrayLocation = NSMutableArray()
    var arrayTitle = NSMutableArray()
    var arrayTime = NSMutableArray()
    var arrayTeam = NSMutableArray()
    var getUserID = NSMutableArray()
    
    var str = String()
    var cell: TableViewCell!
    var arrayKey = NSMutableArray()
    var arrayLatitude = NSMutableArray()
    var arrayLongitude = NSMutableArray()
    
    var strLongitude = Double()
    var strLatitude = Double()
    let locationManager = CLLocationManager()
    var startLocation: CLLocation!
    var lastLocation: CLLocation!
    var startDate: Date!
    var traveledDistance: Double = 0
    var finalArray = NSMutableArray()
    var afterSort = NSMutableArray()
    var currentTimeStamp = Int64()
    var checkProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let timestampp = NSDate().timeIntervalSince1970
//        currentTimeStamp = Int(timestampp)
        
        tableView.frame.size.width = self.view.frame.size.width
        
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.distanceFilter = 10
            
        }
        
       
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
        if startDate == nil {
            startDate = Date()
        } else {
        }
        
        if startLocation == nil {
            startLocation = locations.first
        } else if let location = locations.last {
            traveledDistance += lastLocation.distance(from: location)
            print("Current location updating....******************")
            strLatitude = location.coordinate.latitude
            strLongitude = location.coordinate.longitude
            
        }
        lastLocation = locations.last
    }
    
    
    //MARK: View Will Appear:-
    override func viewWillAppear(_ animated: Bool)
    {
        locationManager.startUpdatingLocation()
        // added inviteBadge = 0
        Database.database().reference().child("users").child((Auth.auth().currentUser?.uid)!).updateChildValues(["InviteBadge": 0])
        let notificationName = Notification.Name("InviteBadgeCountcheck")
        NotificationCenter.default.post(name: notificationName, object: nil)
        if let tblHeightCheck =  UserDefaults.standard.object(forKey: "check") as?CGFloat
        {
            self.view1.frame.size.height = tblHeightCheck
            self.tableView.frame.size.height = self.view1.frame.size.height
            self.lbl.frame.size.height = self.view1.frame.size.height
            self.indicator.center = self.view1.center
            self.indicator.startAnimating()

        }
        self.getUserID.removeAllObjects()
        self.arrayLocation.removeAllObjects()
        self.arrayTime.removeAllObjects()
        self.arrayTitle.removeAllObjects()
        self.arrayTeam.removeAllObjects()
        self.arrayKey.removeAllObjects()
        self.tableView.reloadData()
        
        self.checkProfile = UserDefaults.standard.object(forKey: "checkProfile") as! Bool
        if self.checkProfile == true
        {
            self.getYoursParties()
        }
        else
        {
            self.getInvitedParties()
        }
    }
    
 
//MARK:$$$ Invited Parties :-
    func getInvitedParties()
    {
        let ref = Database.database().reference()
        ref.child("users").child((Auth.auth().currentUser?.uid)!).child("Party").child("Invited").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists()
            {
                    self.lbl.isHidden = true
                    for a in ((snapshot.value as AnyObject).allKeys)!
                    {
                        if self.getUserID.contains(a)
                        {
                            self.getUserID.remove(a)
                        }
                        else
                        {
                            self.getUserID.add(a)
                            self.str = a as! String
                        }
                    }
            }
            else
            {
                self.arrayLocation.removeAllObjects()
                self.arrayTime.removeAllObjects()
                self.arrayTitle.removeAllObjects()
                self.arrayTeam.removeAllObjects()
                self.arrayKey.removeAllObjects()
                self.tableView.reloadData()
                self.indicator.stopAnimating()
                self.lbl.isHidden = false
            }
            self.getUsersCount()
        })

    }
    // Counting users for Invited Parties
    func getUsersCount()
    {
        if getUserID.count != 0
        {
            self.arrayLocation.removeAllObjects()
            self.arrayTime.removeAllObjects()
            self.arrayTitle.removeAllObjects()
            self.arrayTeam.removeAllObjects()
            self.arrayKey.removeAllObjects()
            self.afterSort.removeAllObjects()
            self.finalArray.removeAllObjects()

            for i in 0..<getUserID.count
            {
                let ref1 = Database.database().reference()
                ref1.child("Party").child(self.getUserID[i] as! String).observe( .value, with: { (snapshot)in
                    if snapshot.exists()
                    {
                        var postDict2 = snapshot.value as! [String : AnyObject]
                        postDict2["childKey"] = snapshot.key as AnyObject
                        self.finalArray.add(postDict2)
                        
                        if i == self.getUserID.count-1
                        {
                            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "timeStamp", ascending: false)
                            let sortedarray = self.finalArray.sortedArray(using: [descriptor])
                            self.afterSort.add(sortedarray)
                            self.gettingOutPut()
                        }
                    }
                    else
                    {
                        let ref1 = Database.database().reference()
                        ref1.child("users").child((Auth.auth().currentUser?.uid)!).child("Party").child("Invited").child(snapshot.key).removeValue(completionBlock: {  error in
                        })
                    }

                })
            }
        }
        else
        {
            self.indicator.stopAnimating()
            self.tableView.reloadData()
            self.lbl.isHidden = false
        }
    }
    func gettingOutPut()
    {
        self.lbl.isHidden = true
        let dataarray = afterSort[0] as! NSArray
        for i in 0..<dataarray.count
        {
            let arr = dataarray[i] as! NSDictionary
            // checking Expired Date here
            let demoDate = arr.value(forKey: "TimeForTimeStamp") as! String
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateString = df.date(from: demoDate)
            let since1970: TimeInterval = dateString!.timeIntervalSince1970
            let result = Double(since1970 * 1000)
            
            let dateSt: Int64 = Int64(result)
            
            let timestampp = NSDate().timeIntervalSince1970
            
            self.currentTimeStamp = Int64(timestampp)
            
            if dateSt > currentTimeStamp
            {
                self.arrayKey.add(arr.value(forKey: "childKey")!)
                self.arrayLocation.add(arr.value(forKey: "Location")!)
                self.arrayTeam.add(arr.value(forKey: "Team_Name")!)
                self.arrayTitle.add(arr.value(forKey: "Title")!)
                self.arrayTime.add(arr.value(forKey: "Time")!)
                self.arrayLatitude.add(arr.value(forKey: "location_laitude")!)
                self.arrayLongitude.add(arr.value(forKey: "location_longitude")!)
            }
            else
            {
                
               
            }
            if self.arrayTime.count == 0
            {
                self.lbl.isHidden = false
            }
            else
            {
                self.lbl.isHidden = true
            }

            self.indicator.stopAnimating()
            self.tableView.reloadData()

        }

    }
    override func viewDidDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
        Database.database().reference().removeAllObservers()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayLocation.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        
        cell.frame.size.width = self.view.frame.size.width
        cell.view_cell.layer.shadowColor = UIColor.black.cgColor
        cell.view_cell.layer.shadowOpacity = 1
        cell.view_cell.layer.shadowOffset = CGSize.zero
        cell.view_cell.layer.shadowRadius = 10
        cell.view_cell.layer.cornerRadius = 5
        cell.view_cell.layer.borderColor = UIColor.white.cgColor
        cell.view_cell.layer.borderWidth = 0.5
        cell.view_cell.clipsToBounds = true
        //cell.view_cell.layer.shadowPath = UIBezierPath(rect: cell.view_cell.bounds).cgPath
        cell.view_cell.layer.shouldRasterize = true
        cell.img_icon.layer.cornerRadius = 5;
        cell.img_icon.layer.masksToBounds = true
        cell.img_icon.layer.borderWidth = 1.0
        cell.img_icon.layer.borderColor = UIColor.lightGray.cgColor
        cell.lbl_location.text = arrayLocation[indexPath.row] as? String
        cell.lbl_calender.text = arrayTime[indexPath.row] as? String
        cell.lbl_eventName.text = arrayTitle[indexPath.row] as? String
        cell.lbl_icon_img.text = arrayTeam[indexPath.row] as? String
        
        let myLocation = CLLocation(latitude: strLatitude, longitude: strLongitude)
        
        let myBuddysLocation = CLLocation(latitude: arrayLatitude[indexPath.row] as! CLLocationDegrees, longitude: arrayLongitude[indexPath.row] as! CLLocationDegrees)
        
        let distance = myLocation.distance(from: myBuddysLocation) / 1609
        
      
        cell.lbl_distance.text = String(format: "%.01f Miles", distance)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 99
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        if #available(iOS 9.0, *) {
            if self.checkProfile == true
            {
                cell.selectionStyle = .none
                tableView.allowsSelection = false
            }
            else
            {
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "create_watchParty") as! Create_WatchParty_ViewController
                nextViewController.checkParty = true
                nextViewController.strKey = arrayKey[indexPath.row] as! String
                nextViewController.strTeam = arrayTeam[indexPath.row] as! String
                nextViewController.strTitle = arrayTitle[indexPath.row] as! String
                nextViewController.strTime = arrayTime[indexPath.row] as! String
                nextViewController.strLocation = arrayLocation[indexPath.row] as! String

                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    //MARK:$$$ Yours Parties :-
    func getYoursParties()
    {
        self.indicator.startAnimating()
        var userID = (Auth.auth().currentUser?.uid)!
        self.checkProfile = UserDefaults.standard.object(forKey: "checkProfile") as! Bool
        if self.checkProfile == true
        {
            userID = UserDefaults.standard.object(forKey: "userId") as! String
        }
        let ref = Database.database().reference()
        ref.child("users").child(userID).child("Party").child("Yours").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists()
            {
                self.lbl.isHidden = true
                for a in ((snapshot.value as AnyObject).allKeys)!
                {
                    if self.getUserID.contains(a)
                    {
                        self.getUserID.remove(a)
                    }
                    else
                    {
                        self.getUserID.add(a)
                        self.str = a as! String
                    }
                    
                }
            }
            else
            {
                self.arrayLocation.removeAllObjects()
                self.arrayTime.removeAllObjects()
                self.arrayTitle.removeAllObjects()
                self.arrayTeam.removeAllObjects()
                self.arrayKey.removeAllObjects()
                self.tableView.reloadData()
                
                self.indicator.stopAnimating()
                self.lbl.isHidden = false
            }
            self.getUsersCount_yours()
        })
    }
    
    // Counting users for Invited Parties
    
    func getUsersCount_yours()
    {
        if getUserID.count != 0
        {
            self.arrayLocation.removeAllObjects()
            self.arrayTime.removeAllObjects()
            self.arrayTitle.removeAllObjects()
            self.arrayTeam.removeAllObjects()
            self.arrayKey.removeAllObjects()
            self.afterSort.removeAllObjects()
            self.finalArray.removeAllObjects()
            
            for i in 0..<getUserID.count
            {
                let ref1 = Database.database().reference()
                ref1.child("Party").child(self.getUserID[i] as! String).observe( .value, with: { (snapshot)in
                    if snapshot.exists()
                    {
                        
                        var postDict2 = snapshot.value as! [String : AnyObject]
                        postDict2["childKey"] = snapshot.key as AnyObject
                        self.finalArray.add(postDict2)
                        
                        if i == self.getUserID.count-1
                        {
                            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "timeStamp", ascending: false)
                            let sortedarray = self.finalArray.sortedArray(using: [descriptor])
                            self.afterSort.add(sortedarray)
                            self.gettingOutPut_yours()
                        }
                    }
                    else
                    {
                        let ref1 = Database.database().reference()
                        ref1.child("users").child((Auth.auth().currentUser?.uid)!).child("Party").child("Yours").child(snapshot.key).removeValue(completionBlock: {  error in
                        })
                    }
                    
                })
            }
        }
        else
        {
            self.indicator.stopAnimating()
            self.tableView.reloadData()
            self.lbl.isHidden = false
        }
    }
    func gettingOutPut_yours()
    {
        DispatchQueue.main.async {
            
        self.lbl.isHidden = true
        let dataarray = self.afterSort[0] as! NSArray
        for i in 0..<dataarray.count
        {
            let arr = dataarray[i] as! NSDictionary
            
            // checking Expired Date here
            let demoDate = arr.value(forKey: "TimeForTimeStamp") as! String
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateString = df.date(from: demoDate)
            let since1970: TimeInterval = dateString!.timeIntervalSince1970
            let result = Double(since1970 * 1000)
            
            let dateSt: Int64 = Int64(result)
            
            let timestampp = NSDate().timeIntervalSince1970
            
            let rest = Double(timestampp * 1000)
            self.currentTimeStamp = Int64(rest)

            if dateSt >= self.currentTimeStamp
            {
                self.arrayKey.add(arr.value(forKey: "childKey")!)
                self.arrayLocation.add(arr.value(forKey: "Location")!)
                self.arrayTeam.add(arr.value(forKey: "Team_Name")!)
                self.arrayTitle.add(arr.value(forKey: "Title")!)
                self.arrayTime.add(arr.value(forKey: "Time")!)
                self.arrayLatitude.add(arr.value(forKey: "location_laitude")!)
                self.arrayLongitude.add(arr.value(forKey: "location_longitude")!)
            }
            else
            {
                
            }
            if self.arrayTime.count == 0
            {
                self.lbl.isHidden = false
            }
            else
            {
                self.lbl.isHidden = true
            }
            
            self.indicator.stopAnimating()
            self.tableView.reloadData()
            
        }
        }
    }

    
}
//MARK: ^^^^^^^^^^^^^^^$$$$$$$$$$$$$$$$^^^^^^^^^^^^^Time stamp
//extension Date {
//    var ticks: UInt64 {
//        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
//    }
//}
