//
//  Attending_TableViewCell.swift
//  FanClub
//
//  Created by Brst on 6/9/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class Attending_TableViewCell: UITableViewCell {

    @IBOutlet var view_cell: UIView!
    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var img_eventName: UIImageView!
    @IBOutlet weak var img_location: UIImageView!
    @IBOutlet weak var img_calender: UIImageView!
    @IBOutlet weak var lbl_eventName: UILabel!
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var lbl_calender: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_icon_img: UILabel!
    
    @IBOutlet var btn_chat: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
