//
//  Watch_PartyViewController.swift
//  FanClub
//
//  Created by Brst on 5/17/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseInstanceID

class Watch_PartyViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource  {

    @IBOutlet weak var view_yours: UIView!
    @IBOutlet weak var view_attending: UIView!
    @IBOutlet weak var view_invented: UIView!
    @IBOutlet weak var view_expired: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var lbl_inviteBadge: UILabel!
    
    var pageViewController: UIPageViewController?
    var chart1 : UIViewController!
    var chart2 : UIViewController!
    var chart3 : UIViewController!
    var chart4 : UIViewController!
    var containerHeightCheck = Bool()
    var arrayUserKey = NSMutableArray()
    var arrayUserToken = NSMutableArray()
    var userID = String()
    var checkAttending = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userID = (Auth.auth().currentUser?.uid)!
        
        self.lbl_inviteBadge.layer.cornerRadius = self.lbl_inviteBadge.frame.size.width/2
        self.lbl_inviteBadge.layer.borderWidth = 1
        
        self.lbl_inviteBadge.layer.borderColor = UIColor.init(red: 192/255, green: 57/255, blue: 43/255, alpha: 1).cgColor
        //MARK: Container Height for Table view
        let viewHeight = self.containerView.frame.size.height
        UserDefaults.standard.set(viewHeight, forKey: "check")
        UserDefaults.standard.synchronize()

        chart1 = storyboard?.instantiateViewController(withIdentifier: "yours_watchParty") as? Yours_WatchParty_ViewController
        chart2 =  storyboard?.instantiateViewController(withIdentifier: "profile_attending") as? Profile_AttendingViewController
        chart3 =  storyboard?.instantiateViewController(withIdentifier: "profile_invited") as? Profile_InvitedViewController
        chart4 = storyboard?.instantiateViewController(withIdentifier: "expiredParty") as? WatchParty_ExpiredViewController

        let controllersArray = [chart1!]
        pageViewController = storyboard?.instantiateViewController(withIdentifier: "pageViewController") as? UIPageViewController
        self.pageViewController?.delegate = self
        self.pageViewController?.dataSource = self
        self.pageViewController?.setViewControllers(controllersArray as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        addChildViewController(pageViewController!)
       // pageViewController?.view.frame = CGRect(x:0, y: 0, width: self.view.frame.size.width, height: self.view.frame.height)
        containerView.addSubview((pageViewController?.view)!)
        pageViewController?.didMove(toParentViewController: self)

        let notificationName = Notification.Name("InviteBadgeCountcheck")
        
        NotificationCenter.default.addObserver(self, selector: #selector(Watch_PartyViewController.gettingDetails), name: notificationName, object: nil)
       

    }


    
    func gettingDetails()
    {
        let refo = Database.database().reference()
        refo.child("users").child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists()
            {
                let postDict = snapshot.value as! [String : AnyObject]
                
                // Creating InviteBadges here !!!
                let roomKeys = Array(postDict.keys)
                if roomKeys.contains("InviteBadge")
                {
                    let count = postDict["InviteBadge"] as! Int
                    if count != 0
                    {
                        self.lbl_inviteBadge.text = String(count)
                        self.lbl_inviteBadge.isHidden = false
                    }
                    else
                    {
                        self.lbl_inviteBadge.isHidden = true
                    }
                    
                }
                else
                {
                    refo.child("users").child((Auth.auth().currentUser?.uid)!).updateChildValues(["InviteBadge": 0]) // added inviteBadge = 0
                }
                
            }else
            {
                
            }
            
        }) { (error) in
            print(error.localizedDescription)
            
        }
    }
 

    override func viewWillAppear(_ animated: Bool)
    {
        // chat screen option on Attending Party = true
        UserDefaults.standard.set(checkAttending, forKey: "checkAttending")
    
        let viewHeight = self.containerView.frame.size.height
        UserDefaults.standard.set(viewHeight, forKey: "check")
        UserDefaults.standard.synchronize()
        self.tabBarController?.tabBar.isHidden = false
        self.gettingDetails()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let currentview: UIViewController? = (pageViewController.viewControllers?[0])
        if (currentview is Profile_InvitedViewController) {
            let pvc: Yours_WatchParty_ViewController? = storyboard?.instantiateViewController(withIdentifier: "yours_watchParty") as? Yours_WatchParty_ViewController
            return pvc
        }
        if (viewController is Profile_AttendingViewController) {
            let pvc: Profile_InvitedViewController? = storyboard?.instantiateViewController(withIdentifier: "profile_invited") as? Profile_InvitedViewController
            return pvc
        }
        else {
            return nil
        }
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let currentview: UIViewController? = (pageViewController.viewControllers?[0])

        if (currentview is Yours_WatchParty_ViewController) {
            
            let tvc: Profile_InvitedViewController? = storyboard?.instantiateViewController(withIdentifier: "profile_invited") as? Profile_InvitedViewController
            return tvc
        }

        if (viewController is Profile_AttendingViewController) {
            let tvc: WatchParty_ExpiredViewController? = storyboard?.instantiateViewController(withIdentifier: "expiredParty") as? WatchParty_ExpiredViewController
            return tvc
        }
        if (viewController is Profile_InvitedViewController) {
            let tvc: Profile_AttendingViewController? = storyboard?.instantiateViewController(withIdentifier: "profile_attending") as? Profile_AttendingViewController
            return tvc
        }
          
        if (viewController is WatchParty_ExpiredViewController) {
            let tvc: Yours_WatchParty_ViewController? = storyboard?.instantiateViewController(withIdentifier: "yours_watchParty") as? Yours_WatchParty_ViewController
            return tvc
        }
        else {
            return nil
        }
    }
    public func presentationCount(for pageViewController: UIPageViewController) -> Int // The number of items reflected in the page indicator.
    {
        return 4
    }
    public func presentationIndex(for pageViewController: UIPageViewController) -> Int // The selected item reflected in the page indicator.
    {
        return 0
    }
    
    
    
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        let currentview: UIViewController? = (pageViewController.viewControllers?[0])
        
        if (currentview is Yours_WatchParty_ViewController)
        {
            view_yours.isHidden = false
            view_invented.isHidden = true
            view_attending.isHidden = true
            view_expired.isHidden = true
        }
        if (currentview is Profile_AttendingViewController)
        {
            view_yours.isHidden = true
            view_invented.isHidden = true
            view_attending.isHidden = false
            view_expired.isHidden = true
        }
        if (currentview is Profile_InvitedViewController)
        {
            view_yours.isHidden = true
            view_invented.isHidden = false
            view_attending.isHidden = true
            view_expired.isHidden = true
        }
        if (currentview is WatchParty_ExpiredViewController)
        {
            view_yours.isHidden = true
            view_attending.isHidden = true
            view_invented.isHidden = true
            view_expired.isHidden = false
        }
    }

    @IBAction func watch_attending(_ sender: Any)
    {
        view_yours.isHidden = true
        view_attending.isHidden = false
        view_invented.isHidden = true
        view_expired.isHidden = true
        
        let controllersArray = [chart2!]
        self.pageViewController?.setViewControllers(controllersArray as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }
    @IBAction func watch_yours_selected(_ sender: Any)
    {
        view_yours.isHidden = false
        view_attending.isHidden = true
        view_invented.isHidden = true
        view_expired.isHidden = true

        let controllersArray = [chart1!]
        self.pageViewController?.setViewControllers(controllersArray as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }
    @IBAction func watch_invited(_ sender: Any)
    {
        view_yours.isHidden = true
        view_attending.isHidden = true
        view_invented.isHidden = false
        view_expired.isHidden = true

        let controllersArray = [chart3!]
        self.pageViewController?.setViewControllers(controllersArray as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }
    @IBAction func watch_expired(_ sender: Any)
    {
        view_yours.isHidden = true
        view_attending.isHidden = true
        view_invented.isHidden = true
        view_expired.isHidden = false
        
        let controllerArray = [chart4!]
        self.pageViewController?.setViewControllers(controllerArray as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }

   
   
  
  
}
