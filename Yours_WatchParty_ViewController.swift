//
//  Yours_WatchParty_ViewController.swift
//  FanClub
//
//  Created by Brst on 5/17/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore
import MapKit

class Yours_WatchParty_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , CLLocationManagerDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var lbl: UILabel!
    @IBOutlet var view1: UIView!
    
    
    var arrayLocation = NSMutableArray()
    var arrayTitle = NSMutableArray()
    var arrayTime = NSMutableArray()
    var arrayTeam = NSMutableArray()
    var getUserID = NSMutableArray()
    var str = String()
    var cell: Yours_TableViewCell!
    var arrayKey = NSMutableArray()

    var arrayLatitude = NSMutableArray()
    var arrayLongitude = NSMutableArray()
    
    var strLongitude = Double()
    var strLatitude = Double()

    let locationManager = CLLocationManager()
    var startLocation: CLLocation!
    var lastLocation: CLLocation!
    var startDate: Date!
    var traveledDistance: Double = 0
    var finalArray = NSMutableArray()
    var afterSort = NSMutableArray()
    var currentTimeStamp = Int64()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        let timestampp = NSDate().timeIntervalSince1970
//        currentTimeStamp = Int(timestampp)
        
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(reset), userInfo: nil, repeats: false)
       
        tableView.register(UINib(nibName: "Yours_TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.distanceFilter = 10
        }
        
    }
    
    func reset()  {
        if  let tblHeightCheck =  UserDefaults.standard.object(forKey: "check") as? CGFloat
        {
            self.view1.frame.size.height = tblHeightCheck
            self.tableView.frame.size.height = self.view1.frame.size.height
            self.lbl.frame.size.height = self.view1.frame.size.height
            self.indicator.center = self.view1.center
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if startDate == nil {
            startDate = Date()
        } else {
        }
        
        if startLocation == nil {
            startLocation = locations.first
        } else if let location = locations.last {
            traveledDistance += lastLocation.distance(from: location)
            print("Current location updating....******************")
            
            strLatitude = location.coordinate.latitude
            strLongitude = location.coordinate.longitude
        }
        lastLocation = locations.last
    }
    
    
    //MARK: view will appear
    override func viewWillAppear(_ animated: Bool)
    {
        locationManager.startUpdatingLocation()
        if let tblHeightCheck =  UserDefaults.standard.object(forKey: "check") as? Int
        {
            self.view1.frame.size.height = CGFloat(tblHeightCheck)
            self.tableView.frame.size.height = self.view1.frame.size.height
            self.lbl.frame.size.height = self.view1.frame.size.height
            self.indicator.center = self.view1.center
            self.indicator.startAnimating()

        }
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(reset), userInfo: nil, repeats: false)

        self.arrayLocation.removeAllObjects()
        self.arrayTime.removeAllObjects()
        self.arrayTitle.removeAllObjects()
        self.arrayTeam.removeAllObjects()
        self.getUserID.removeAllObjects()
        self.afterSort.removeAllObjects()
        self.finalArray.removeAllObjects()
        self.tableView.reloadData()
        
        self.getYoursParties()
    }
   
    //MARK:$$$ Attended Parties :-
    func getYoursParties()
    {
        self.indicator.startAnimating()
        let ref = Database.database().reference()
        ref.child("users").child((Auth.auth().currentUser?.uid)!).child("Party").child("Yours").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists()
            {
                self.lbl.isHidden = true
                for a in ((snapshot.value as AnyObject).allKeys)!
                {
                    if self.getUserID.contains(a)
                    {
                        self.getUserID.remove(a)
                    }
                    else
                    {
                        self.getUserID.add(a)
                        self.str = a as! String
                    }
       
                }
            }
            else
            {
                self.arrayLocation.removeAllObjects()
                self.arrayTime.removeAllObjects()
                self.arrayTitle.removeAllObjects()
                self.arrayTeam.removeAllObjects()
                self.arrayKey.removeAllObjects()
                self.tableView.reloadData()

                self.indicator.stopAnimating()
                self.lbl.isHidden = false
            }
            self.getUsersCount()
        })
    }

    // Counting users for Invited Parties
    
    func getUsersCount()
    {
        if getUserID.count != 0
        {
            self.afterSort.removeAllObjects()
            self.finalArray.removeAllObjects()
            
            for i in 0..<getUserID.count
            {
                let ref1 = Database.database().reference()
                ref1.child("Party").child(self.getUserID[i] as! String).observe( .value, with: { (snapshot)in
                    if snapshot.exists()
                    {
                        
                        var postDict2 = snapshot.value as! [String : AnyObject]
                        postDict2["childKey"] = snapshot.key as AnyObject
                        self.finalArray.add(postDict2)
                        
                        if i == self.getUserID.count-1
                        {
                            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "timeStamp", ascending: false)
                            let sortedarray = self.finalArray.sortedArray(using: [descriptor])
                            self.afterSort.add(sortedarray)
                            self.gettingOutPut()
                        }
                    }
                    else
                    {
                        let ref1 = Database.database().reference()
                         ref1.child("users").child((Auth.auth().currentUser?.uid)!).child("Party").child("Yours").child(snapshot.key).removeValue(completionBlock: {  error in
                        })
                    }
                    
                })
            }
        }
        else
        {
            self.indicator.stopAnimating()
            self.tableView.reloadData()
            self.lbl.isHidden = false
        }
    }
    func gettingOutPut()
    {
        self.arrayLocation.removeAllObjects()
        self.arrayTime.removeAllObjects()
        self.arrayTitle.removeAllObjects()
        self.arrayTeam.removeAllObjects()
        self.arrayKey.removeAllObjects()
        self.lbl.isHidden = true
        let dataarray = afterSort[0] as! NSArray
        for i in 0..<dataarray.count
        {
            let arr = dataarray[i] as! NSDictionary
            
            // checking Expired Date here
            let demoDate = arr.value(forKey: "TimeForTimeStamp") as! String
       
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateString = df.date(from: demoDate)
            let since1970: TimeInterval = dateString!.timeIntervalSince1970
            let result = Double(since1970 * 1000)
        
            let dateSt: Int64 = Int64(result)
    
            let timestampp = NSDate().timeIntervalSince1970
            let rest = Double(timestampp * 1000)
            self.currentTimeStamp = Int64(rest)
//            if dateSt > self.currentTimeStamp
//            {
//                print("\(dateSt)\(" - ")\(self.currentTimeStamp)")
//            }
//            else
//            {
//                print("\(self.currentTimeStamp)\(" - ")\(dateSt)")
//            }
            
            if  dateSt > self.currentTimeStamp    //dateSt > currentTimeStamp
            {
                self.arrayKey.add(arr.value(forKey: "childKey")!)
                self.arrayLocation.add(arr.value(forKey: "Location")!)
                self.arrayTeam.add(arr.value(forKey: "Team_Name")!)
                self.arrayTitle.add(arr.value(forKey: "Title")!)
                self.arrayTime.add(arr.value(forKey: "Time")!)
                self.arrayLatitude.add(arr.value(forKey: "location_laitude")!)
                self.arrayLongitude.add(arr.value(forKey: "location_longitude")!)
            }
            else
            {
                // print("Not expired :=\(dateSt)\(" < ")\(currentTimeStamp)")
            }
            if self.arrayTime.count == 0
            {
                self.lbl.isHidden = false
            }
            else
            {
                self.lbl.isHidden = true
            }
            
            
//            self.arrayKey.add(arr.value(forKey: "childKey")!)
//            self.arrayLocation.add(arr.value(forKey: "Location")!)
//            self.arrayTeam.add(arr.value(forKey: "Team_Name")!)
//            self.arrayTitle.add(arr.value(forKey: "Title")!)
//            self.arrayTime.add(arr.value(forKey: "Time")!)
            
//            let dateString = "2014-01-31" //"20 June 2017 2:13 PM" //"2014-01-31 17:40:56"
//            let df = DateFormatter()
//            df.dateFormat = "yyyy-MM-dd"   //"dd MMMM yyyy hh:mm a Z"    //"yyyy-MM-dd HH:mm:ss"
//            df.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
//            let date: Date? = df.date(from: dateString)
//            let since1970: TimeInterval? = date?.timeIntervalSince1970
//            // January 1st 1970
//            let result = Double(since1970!)
//
//            print(result)
//            self.arrayLatitude.add(arr.value(forKey: "location_laitude")!)
//            self.arrayLongitude.add(arr.value(forKey: "location_longitude")!)
            self.indicator.stopAnimating()
            self.tableView.reloadData()
            
        }
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
        Database.database().reference().removeAllObservers()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func chatScreenOpen(sender: UIButton)
    {
      //  Database.database().reference().child("Party").child(self.arrayKey[sender.tag] as! String).updateChildValues(["PartyChat": ""])
        
        UserDefaults.standard.set(self.arrayTitle[sender.tag], forKey: "partyName")
        UserDefaults.standard.set(self.arrayKey[sender.tag], forKey: "partyKey")
      //  let notificationNameChat = Notification.Name("chatScreenNotification")
       // NotificationCenter.default.post(name: notificationNameChat, object: nil)
        self.tabBarController?.tabBar.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let walkthroughVC = storyboard.instantiateViewController(withIdentifier: "chatScreen") as! ChatScreen_ViewController
        self.navigationController?.pushViewController(walkthroughVC, animated: false)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayLocation.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! Yours_TableViewCell
        
        cell.frame.size.width = self.view.frame.size.width
        cell.view_cell.layer.shadowColor = UIColor.black.cgColor
        cell.view_cell.layer.shadowOpacity = 1
        cell.view_cell.layer.shadowOffset = CGSize.zero
        cell.view_cell.layer.shadowRadius = 10
        cell.view_cell.layer.cornerRadius = 5
        cell.view_cell.layer.borderColor = UIColor.white.cgColor
        cell.view_cell.layer.borderWidth = 0.5
        cell.view_cell.clipsToBounds = true
        //cell.view_cell.layer.shadowPath = UIBezierPath(rect: cell.view_cell.bounds).cgPath
        cell.view_cell.layer.shouldRasterize = true
        cell.img_icon.layer.cornerRadius = 5;
        cell.img_icon.layer.masksToBounds = true
        cell.img_icon.layer.borderWidth = 1.0
        cell.img_icon.layer.borderColor = UIColor.lightGray.cgColor
        cell.lbl_location.text = arrayLocation[indexPath.row] as? String
        cell.lbl_calender.text = arrayTime[indexPath.row] as? String
        cell.lbl_eventName.text = arrayTitle[indexPath.row] as? String
        cell.lbl_icon_img.text = arrayTeam[indexPath.row] as? String
        
        let myLocation = CLLocation(latitude: strLatitude, longitude: strLongitude)
        
        let myBuddysLocation = CLLocation(latitude: arrayLatitude[indexPath.row] as! CLLocationDegrees, longitude: arrayLongitude[indexPath.row] as! CLLocationDegrees)
        
        let distance = myLocation.distance(from: myBuddysLocation) / 1609
        
        
        cell.lbl_distance.text = String(format: "%.01f Miles", distance)
        
        // chat screen added 
        cell.btn_chat.tag = indexPath.row
        cell.btn_chat.addTarget(self, action: #selector(chatScreenOpen), for: .touchUpInside)

        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 99
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        if #available(iOS 9.0, *) {
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "create_watchParty") as! Create_WatchParty_ViewController
            nextViewController.deleteParty = true
            nextViewController.strKey = arrayKey[indexPath.row] as! String
            nextViewController.strTeam = arrayTeam[indexPath.row] as! String
            nextViewController.strTitle = arrayTitle[indexPath.row] as! String
            nextViewController.strTime = arrayTime[indexPath.row] as! String
            nextViewController.strLocation = arrayLocation[indexPath.row] as! String
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        } else {
            // Fallback on earlier versions
        }
    }

}
