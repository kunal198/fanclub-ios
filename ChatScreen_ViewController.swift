//
//  ChatScreen_ViewController.swift
//  FanClub
//
//  Created by tbi-pc-57 on 11/16/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseInstanceID
import IQKeyboardManagerSwift

class ChatScreen_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, URLSessionDelegate, UITextFieldDelegate {

    @IBOutlet var view_msgScreen: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var view_chatview: UIView!
    @IBOutlet var txt_chat: UITextField!
    @IBOutlet var btn_sendMsg: UIButton!
    @IBOutlet var lbl_partyName: UILabel!
    
    var cell: TableViewCell_ChatScreen!
    var CurrentUserName = String()
    var receiverName = String()
    var arrayUserKey = NSMutableArray()
    var arrayUserToken = NSMutableArray()
    var partyName = String()
    var partyKey = String()
    var arrayFanKey = NSMutableArray()
    var msgArray = NSMutableArray()
    var userID = String()
    var checkAttending = true
    var appGoesToBackground = Bool()
    var messageViewFrame = CGRect()
    var chatViewFrame = CGRect()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.userID = (Auth.auth().currentUser?.uid)!
        self.tabBarController?.tabBar.isHidden = true
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 62.0;
        
        self.btn_sendMsg.layer.cornerRadius = self.btn_sendMsg.frame.size.width/2
        self.btn_sendMsg.layer.borderWidth = 1
        self.btn_sendMsg.layer.borderColor = UIColor.yellow.cgColor
        self.chatScreenOpen()
//        let notificationName = Notification.Name("InviteBadgeCountcheck")
//
//        NotificationCenter.default.addObserver(self, selector: #selector(Watch_PartyViewController.gettingDetails), name: notificationName, object: nil)
        
//        let notificationNameChat = Notification.Name("chatScreenNotification")
//
//        NotificationCenter.default.addObserver(self, selector: #selector(Watch_PartyViewController.chatScreenOpen), name: notificationNameChat, object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatScreen_ViewController.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatScreen_ViewController.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.callViewWillAppear(notification:)), name: .UIApplicationWillEnterForeground, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.callViewWilldisaappear(notification:)), name: .UIApplicationDidEnterBackground, object: nil)
        
        
    }
    //MARK: View will appear function here !!
    override func viewWillAppear(_ animated: Bool) {
        // chat screen option on Attending Party = true
        UserDefaults.standard.set(checkAttending, forKey: "checkAttending")
        IQKeyboardManager.sharedManager().enable = false
        
        messageViewFrame = self.view_chatview.frame
        chatViewFrame = tableView.frame
    }
    //MARK: View Will Disappear function here !!
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enable = true
        Database.database().reference().removeAllObservers()
        UserDefaults.standard.removeObject(forKey: "partyEventScreen")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callViewWilldisaappear(notification: NSNotification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        appGoesToBackground = true
    }
    
    func callViewWillAppear(notification: NSNotification)
    {
        appGoesToBackground = false
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatScreen_ViewController.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatScreen_ViewController.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        //if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
        //  print("Show")
        if appGoesToBackground == false && messageViewFrame.origin.y == view_chatview.frame.origin.y
        {
            let keyboardSize = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
            view_chatview.frame.origin.y -= keyboardSize.height
            tableView.frame.size.height -= keyboardSize.height
        }
        
        // }
    }
    func keyboardWillHide(notification: NSNotification) {
        // if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
        // print("Hide")
        
        if appGoesToBackground == false
        {
            let keyboardSize = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
            view_chatview.frame.origin.y += keyboardSize.height
            tableView.frame.size.height += keyboardSize.height
        }
        
        // }
    }
    
    
    
    
    func chatScreenOpen()
    {
        self.gettingUserCount()
        if UserDefaults.standard.object(forKey: "partyKey") as! String != "" || UserDefaults.standard.object(forKey: "partyName") as! String != ""
        {
            self.msgArray.removeAllObjects()
            self.tableView.reloadData()
            
            partyKey = UserDefaults.standard.object(forKey: "partyKey") as! String
            
            partyName = UserDefaults.standard.object(forKey: "partyName") as! String
            self.lbl_partyName.text = partyName
            
        Database.database().reference().child("Party").child(self.partyKey).child("PartyChat").observe(.childAdded, with: { (snapshot) in
            
            let array = NSMutableArray()
            
            if snapshot.hasChildren() == true
            {
                let postDict2 = snapshot.value as! [String : AnyObject]
                array.add(postDict2)
                if self.msgArray.contains(array[0] as! NSDictionary)
                {
                    
                }
                else
                {
                    self.msgArray.add(array[0] as! NSDictionary)
                }
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                if self.msgArray.count != 0
                {
                    let lastItem = IndexPath(item: self.msgArray.count - 1, section: 0)
                    self.tableView.scrollToRow(at: lastItem, at: .bottom, animated: true)
                    
                }
                }
            })
            
            //   }
            //  })
            
        }
        
    }
    
    @IBAction func btn_chatScreenClose(_ sender: Any)
    {
        view.endEditing(true)
        Database.database().reference().removeAllObservers()
        self.tabBarController?.tabBar.isHidden = false

        if UserDefaults.standard.object(forKey: "notiPartyKey") != nil
        {
            // make another root view controller , coming form app delegate
            UserDefaults.standard.removeObject(forKey: "notiPartyKey")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let walkthroughVC = storyboard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
            walkthroughVC.selectedIndex = 1
            let navigationController = self.view.window?.rootViewController as! UINavigationController
            
            navigationController.pushViewController(walkthroughVC, animated: false)
        }
        else
        {
            self.navigationController?.popViewController(animated: false)
        }
        
       /*
        if UserDefaults.standard.object(forKey: "partyEventScreen") != nil
        {
            //            UserDefaults.standard.removeObject(forKey: "partyEventScreen")
            //            self.navigationController?.popViewController(animated: true)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            if #available(iOS 9.0, *) {
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "create_watchParty") as! Create_WatchParty_ViewController
                nextViewController.notAttending = true
                self.navigationController?.pushViewController(nextViewController, animated: true)
            } else {
                // Fallback on earlier versions
                // self.tabBarController?.tabBar.isHidden = false
            }
            
            
        }
        else
        {
            self.tabBarController?.tabBar.isHidden = false
        }
        //        if UserDefaults.standard.object(forKey: "notiPartyKey") != nil
        //        {
        //            UserDefaults.standard.removeObject(forKey: "notiPartyKey")
        //            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        //            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
        //            self.navigationController?.pushViewController(nextViewController, animated: true)
        //        }
        */
    }
    
    //MARK: Table View Delegates : Chat Screen
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.msgArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1") as! Yours_TableViewCell
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "cell2") as! Yours_TableViewCell
        
        // print(self.msgArray)
        
        let arr = self.msgArray[indexPath.row] as! NSDictionary
        
        if arr.value(forKey: "userId") as! String == self.userID
        {
            cell2.lbl_senderName.text = arr.value(forKey: "userName") as? String
            cell2.lbl_senderMsg.text = arr.value(forKey: "msg") as? String
            return cell2
        }
        cell1.lbl_receiverName.text = arr.value(forKey: "userName") as? String
        cell1.lbl_receiverMsg.text = arr.value(forKey: "msg")  as? String
        return cell1
    }

    //MARK: send msg in the group
    
    @IBAction func btn_addMSg(_ sender: Any)
    {
        var msgTxt = String()
        CurrentUserName = UserDefaults.standard.object(forKey: "CurrentUserName") as! String
        if self.txt_chat.text != ""
        {
            msgTxt = self.txt_chat.text!
            self.txt_chat.text = ""
            //  let timestampp = NSDate().timeIntervalSince1970
            //  let rest = Double(timestampp * 1000)
            //let timeStampFnl = Int64(rest)
            
            let partyKey = UserDefaults.standard.object(forKey: "partyKey") as! String
            Database.database().reference().child("Party").child(partyKey).child("PartyChat").childByAutoId().setValue(["userId": (Auth.auth().currentUser?.uid)!, "userName": self.CurrentUserName, "msg": msgTxt,"TimeStamp": "timeStampFnl"])
            
            // view.endEditing(true)
            
            for i in 0..<self.arrayUserToken.count
            {
                let token = self.arrayUserToken[i] as! String
                //  print(token)
                // self.sendNotifications(msgTxt, andToken: token)
                self.sendPushMessage(msgTxt, andToken: (token))
            }
        }
    }
    
    func sendPushMessage(_ message: String, andToken token: String){
        
        let url  = NSURL(string: "https://fcm.googleapis.com/fcm/send")
        
        let refreshedToken = InstanceID.instanceID().token()
        
        
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("key=AAAAF-ZTlZQ:APA91bFmIGBSDDmqjcckCLQnfmcJDlbKjt03QwYGVCRVsnrmlDrsxzNwxgJYhtEnrrUzBPw5PsJ5POBBaeMNX4_v8lMk93SelB9HdPyapvO6UNQCgBiEFrcgM-TJtkreU0nPVlr94utT", forHTTPHeaderField:"Authorization") // your legacy server key
        request.setValue(refreshedToken, forHTTPHeaderField:"Authentification") //  sender refreshedToken
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.httpMethod = "POST"
        let sessionConfig = URLSessionConfiguration.default
        
        let json = ["to":(token),
                    "priority":"high",
                    "content_available":true,
                    "data":["ttbadge": 1,"PartyKey":partyKey,"message":message,"PartyName":partyName],
                    "notification":["ttbadge":1,"body": message,"time": Int(Date().timeIntervalSince1970)]] as [String : Any]
        
        //  print(json)
        
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            request.httpBody = jsonData
            
        } catch let error as NSError {
            print(error)
        }
        
        // insert json data to the request
        
        let urlSession = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: OperationQueue.main)
        
        
        
        let task = urlSession.dataTask(with: request as URLRequest, completionHandler: {
            (
            data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                print("error")
                return
            }
            
           // let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            //  print("\(String(describing: dataString))")
            
        })
        task.resume()
        
    }
    
    
    // getting usercount for token it
    func gettingUserCount()
    {
        let partyKey = UserDefaults.standard.object(forKey: "partyKey") as! String
        self.arrayFanKey.removeAllObjects()
        Database.database().reference().child("Party").child(partyKey).child("receiverID").observeSingleEvent(of: .value, with: { (snapshot) in
            //   print(snapshot)
            if snapshot.hasChildren() == true
            {
                for a in ((snapshot.value as AnyObject).allKeys)!
                {
                    self.txt_chat.isUserInteractionEnabled = true
                    let str = a as! String
                    //  print(str)
                    if str != (Auth.auth().currentUser?.uid)!
                    {
                        self.arrayFanKey.add(str)
                    }
                }
                self.gettingUserToken()
            }
            else
            {
                self.txt_chat.isUserInteractionEnabled = false
            }
        })
    }
    
    func gettingUserToken()
    {
        if self.arrayFanKey.count != 0
        {
            self.arrayUserToken.removeAllObjects()
            for i in 0..<self.arrayFanKey.count
            {
                Database.database().reference().child("users").child(self.arrayFanKey[i] as! String).observeSingleEvent(of: .value, with: {(snapshot) in
                    if snapshot.exists()
                    {
                        //   print(snapshot)
                        
                        let dict = snapshot.value as! [String : AnyObject]
                        if dict["DeviceToken"] as? String != nil
                        {
                            let token = dict["DeviceToken"] as! String
                            self.arrayUserToken.add(token)
                        }
                        
                    }
                    else
                    {
                        // print("no DAta")
                    }
                })
            }
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "No Fans To Chat", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                self.navigationController?.popViewController(animated: false)
                self.tabBarController?.tabBar.isHidden = false
            }
            
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
   /*
    func sendNotifications(_ message: String, andToken token: String)
    {
        let timestampp = NSDate().timeIntervalSince1970
        let rest = Double(timestampp * 1000)
        let timeStampFnl = Int64(rest)
        
        let partyKey = UserDefaults.standard.object(forKey: "partyKey") as! String
        if let refreshedToken = InstanceID.instanceID().token()
        {
            //   print("InstanceID token: \(refreshedToken)")
            //   print(timeStampFnl)
            //   print(token)
            //   print(message)
            //   print(partyName)
            //   print(partyKey)
            
            
            /*   let post = ["to":token,
             "priority":"high",
             "content_available":true,
             "data":["PartyName": "\(partyKey) sent you a photo.","message":message,"other_user_id":refreshedToken,"coming_from_about":"messages"],
             "notification":["body": "\(partyName) sent you a photo.","time": Int(Date().timeIntervalSince1970)]] as [String : Any]
             */
            
            
            
            //            let json = ["to":(token),
            //                        "priority":"high",
            //                        "content_available":true,
            //                        "data":["title": "User sent you a message.","PartyKey":partyKey,"message":message,"PartyName":partyName],
            //                        "notification":["badge":1,"body": message,"time": Int(Date().timeIntervalSince1970)]] as [String : Any]
            //
            //            print(json)
            
            let badgeCount = 1
            let title = "User sent you a message."
            //            \"time\":\"\(Int(Date().timeIntervalSince1970))\"
            
            //            let post = NSString(format:"{\"to\":\"\(token)\",\"notification\":{\"badge\":\"\(badgeCount)\",\"body\":\"\(message)\",\"title\":\"\(title)\",\"PartyKey\":\"\(partyKey)\",\"message\":\"\(message)\",\"PartyName\":\"\(partyName)\"},\"priority\":10}" as NSString)
            
            let post = NSString(format:"{\"to\":\"\(token)\",\"notification\":{\"badge\":\"\(badgeCount)\",\"body\":\"\(message)\", \"title\":\"\(title)\", \"PartyKey\":\"\(partyKey)\", \"PartyName\":\"\(partyName)\"},\"priority\":10}" as NSString)
            
            
            //   print(post)
            
            
            
            //            var dataModel = post.data(using: String.Encoding.ascii.rawValue)!
            var dataModel = post.data(using:  String.Encoding.nonLossyASCII.rawValue, allowLossyConversion: true)!
            let postLength = String(dataModel.count)
            let url = NSURL(string: "https://fcm.googleapis.com/fcm/send")
            let urlRequest = NSMutableURLRequest(url: url! as URL)
            urlRequest.httpMethod = "POST"
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue("key=AAAAF-ZTlZQ:APA91bFmIGBSDDmqjcckCLQnfmcJDlbKjt03QwYGVCRVsnrmlDrsxzNwxgJYhtEnrrUzBPw5PsJ5POBBaeMNX4_v8lMk93SelB9HdPyapvO6UNQCgBiEFrcgM-TJtkreU0nPVlr94utT", forHTTPHeaderField: "Authorization")
            urlRequest.httpBody = dataModel
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            let task = URLSession.shared.dataTask(with: urlRequest as URLRequest) { (data, response, error) -> Void in
                if let urlContent = data {
                    do {
                        let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers)
                        //  print(jsonResult)
                        
                    } catch {
                        print("JSON serialization failed = \(error.localizedDescription)")
                    }
                    
                } else {
                    print("ERROR FOUND HERE = \(error?.localizedDescription)")
                }
            }
            task.resume()
        }
    }
    */
    
    //MARK: Text field delegate
    
    func textFieldShouldReturn(_ txt_chat: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
    
    
}
