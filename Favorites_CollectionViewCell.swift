//
//  Favorites_CollectionViewCell.swift
//  FanClub
//
//  Created by Brst on 5/18/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class Favorites_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lbl: UILabel!
    @IBOutlet var img: UIImageView!
    
}
