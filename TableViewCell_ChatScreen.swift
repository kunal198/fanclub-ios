//
//  TableViewCell_ChatScreen.swift
//  FanClub
//
//  Created by Brst on 10/27/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class TableViewCell_ChatScreen: UITableViewCell {

    @IBOutlet var lbl_title: UILabel!
    @IBOutlet var lbl_msg: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
