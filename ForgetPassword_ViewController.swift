//
//  ForgetPassword_ViewController.swift
//  FanClub
//
//  Created by Brst on 5/23/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore

class ForgetPassword_ViewController: UIViewController , UITextFieldDelegate{

    @IBOutlet var view_email: UIView!
    @IBOutlet var txt_forgetPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_email.layer.cornerRadius = 5;
        view_email.layer.masksToBounds = true
        view_email.layer.borderWidth = 1.0
        view_email.layer.borderColor = UIColor.lightGray.cgColor
    }

    @IBAction func forget_password(_ sender: Any)
    {
        if txt_forgetPassword.text != ""
        {
            let email = txt_forgetPassword.text
            Auth.auth().sendPasswordReset(withEmail: email!) { error in
                if error != nil
                {
                    let alert = UIAlertController(title: "Error Occur", message: "Please check your email address", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                
                }
                else {
                        let alert:UIAlertController = UIAlertController(title: "Email Sent", message: "An email has been sent. Please, check your email now", preferredStyle: UIAlertControllerStyle.alert)
                        let loginAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.default)
                        {
                            UIAlertAction in
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "logIn") as! LoginViewController
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                        alert.addAction(loginAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Enter Email ID", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
// MARK : text field functions
    private func textFieldDidEndEditing(_ textField: UITextField)-> Bool {
        
        return true;
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.view.frame.origin.y = 0;
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }

    @IBAction func back_btn(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    }
