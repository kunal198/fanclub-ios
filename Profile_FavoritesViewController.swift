//
//  Profile_FavoritesViewController.swift
//  FanClub
//
//  Created by Brst on 5/16/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore

class Profile_FavoritesViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource{

    @IBOutlet var lbl: UILabel!
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var view1: UIView!
    
    var dataNFL = NSMutableArray()
    var dataNBA = NSMutableArray()
    var dataMLB = NSMutableArray()
    var dataNCCA = NSMutableArray()
    var getNFL = NSMutableArray()
    var getNBA = NSMutableArray()
    var getMLB = NSMutableArray()
    var getNCCA = NSMutableArray()
    var gotData = Bool()
    var str1 = String()
    var checkProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(reset), userInfo: nil, repeats: false)

    }
    override func viewWillAppear(_ animated: Bool)
    {
        if let collectionHeightCheck =  UserDefaults.standard.object(forKey: "check") as? CGFloat
        {
            self.view1.frame.size.height = collectionHeightCheck
            self.collectionView.frame.size.height = CGFloat(collectionHeightCheck - 5)
            self.lbl.frame.size.height = self.view1.frame.size.height
            self.indicator.center = self.view1.center
            self.indicator.startAnimating()

        }
        self.getNFL.removeAllObjects()
        
        var userID = (Auth.auth().currentUser?.uid)!
        self.checkProfile = UserDefaults.standard.object(forKey: "checkProfile") as! Bool
        if self.checkProfile == true
        {
            userID = UserDefaults.standard.object(forKey: "userId") as! String
        }

        let ref = Database.database().reference()
        ref.child("Sports").child(userID).observe( .value, with: { (snapshot) in
            if snapshot.exists()
            {
                self.collectionView.isHidden = false
                self.lbl.isHidden = true

                var postDict = snapshot.value as! [String : AnyObject]
                
                let array = postDict["NFL"] as? NSMutableArray
                if array == nil
                {

                }
                else
                {
                    self.getNFL.addObjects(from: array as! [Any])

                }
                
                let array1 = postDict["NBA"] as? NSMutableArray
                if array1 == nil
                {

                }
                else
                {
                    self.getNFL.addObjects(from: array1 as! [Any])
                }
                
                let array2 = postDict["MLB"] as? NSMutableArray
                if array2 == nil
                {

                }
                else
                {
                    self.getNFL.addObjects(from: array2 as! [Any])
                }
                
                let array3 = postDict["NCCA_Football"] as? NSMutableArray
                if array3 == nil
                {

                }
                else
                {
                    self.getNFL.addObjects(from: array3 as! [Any])
                }
                self.indicator.stopAnimating()
                self.collectionView.reloadData()

            }
            else
            {
                self.indicator.stopAnimating()
                self.collectionView.isHidden = true
                self.lbl.isHidden = false
                self.getNFL.removeAllObjects()
                self.collectionView.reloadData()
            }
            
        })
        
    }
    func reset()  {
        if let tblHeightCheck =  UserDefaults.standard.object(forKey: "check") as? Int
        {
            //UserDefaults.standard.synchronize()
            self.view1.frame.size.height = CGFloat(tblHeightCheck)
            self.collectionView.frame.size.height = self.view1.frame.size.height
            self.lbl.frame.size.height = self.view1.frame.size.height
            self.indicator.center = self.view1.center
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//MARK: Collection View Cell Functions :-
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return getNFL.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        cell.layer.cornerRadius = 5;
        cell.layer.masksToBounds = true
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.lbl.text = self.getNFL[indexPath.row] as? String
        return cell
    }
  
}
