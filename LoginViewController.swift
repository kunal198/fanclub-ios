//
//  LoginViewController.swift
//  FanClub
//
//  Created by Brst on 5/16/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore
import UserNotifications
import FirebaseMessaging
import FirebaseInstanceID

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var view_email_login: UIView!
    @IBOutlet weak var view_password_login: UIView!
    @IBOutlet var txt_email: UITextField!
    @IBOutlet var txt_password: UITextField!
    var loadingIndicator = UIActivityIndicatorView()
    
    @IBAction func login_toEnter(_ sender: Any)
    {
        loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        loadingIndicator.center = self.view.center
        loadingIndicator.color = UIColor.black
        loadingIndicator.startAnimating()
        self.loadingIndicator.hidesWhenStopped = true
        
        if txt_password.text == "" && txt_email.text == ""
        {
            self.loadingIndicator.stopAnimating()
            let alertController = UIAlertController(title: "Error", message: "Please enter valid email Id and password", preferredStyle: .alert)
        
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            
            loadingIndicator.startAnimating()
            self.view.addSubview(loadingIndicator)
          
            
            Auth.auth().signIn(withEmail: self.txt_email.text!, password: self.txt_password.text!) { (user, error) in
                if error == nil {
                    let userID = Auth.auth().currentUser?.uid
                    UserDefaults.standard.set(userID, forKey: "userID")
                    
                    // firebase message token
                    let refreshedToken = InstanceID.instanceID().token()
                    UserDefaults.standard.set(refreshedToken, forKey: "fcmToken")
                    
                    let notificationName = Notification.Name("addToken")
                    NotificationCenter.default.post(name: notificationName, object: nil)
                    
                    self.connectToFcm()
                    
                    print("You have successfully logged in")
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let walkthroughVC = storyboard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
                   // walkthroughVC.selectedIndex = 1
                    let navigationController = self.view.window?.rootViewController as! UINavigationController
                    
                    navigationController.pushViewController(walkthroughVC, animated: false)
                    
                    
              /*      let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
                    self.view.window?.rootViewController = nextViewController
                    
                    self.navigationController?.pushViewController(nextViewController, animated: true)*/
                    
                } else {
                    
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                
                self.loadingIndicator.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true;
        
        view_email_login.layer.cornerRadius = 5;
        view_email_login.layer.masksToBounds = true
        view_email_login.layer.borderWidth = 1.0
        view_email_login.layer.borderColor = UIColor.lightGray.cgColor
        view_password_login.layer.cornerRadius = 5;
        view_password_login.layer.masksToBounds = true
        view_password_login.layer.borderWidth = 1.0
        view_password_login.layer.borderColor = UIColor.lightGray.cgColor
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Text Field Functions :-
    private func textFieldDidEndEditing(_ textField: UITextField)-> Bool {
        
        return true;
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.view.frame.origin.y = 0;
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    @IBAction func back_btn(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func forgetPassword_value(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "forgetPassword") as! ForgetPassword_ViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
   
    
    func connectToFcm() {
        Messaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
                // Messaging.messaging().subscribe(toTopic: "/topics/default-notifications-channel")
            }
        }
    }
}
