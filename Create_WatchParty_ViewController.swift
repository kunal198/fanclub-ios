//
//  Create_WatchParty_ViewController.swift
//  FanClub
//
//  Created by Brst on 5/18/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore
import SDWebImage

protocol HandleMapSearch: class {
    func dropPinZoomIn(_ placemark:MKPlacemark)
}

@available(iOS 9.0, *)
class Create_WatchParty_ViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource , UITextFieldDelegate{
    
    @IBOutlet var indicator_location: UIActivityIndicatorView!
    @IBOutlet var btn_location_click: UIButton!
    @IBOutlet var lbl_watchParty: UILabel!
    @IBOutlet var btn_selectTeam: UIButton!
    @IBOutlet var txt_title: UITextField!
    @IBOutlet var btn_back: UIButton!
    @IBOutlet var btn_save_click: UIButton!
    @IBOutlet var img_selectedTeam: UIImageView!
    @IBOutlet var lbl_selectedTeam: UILabel!
    @IBOutlet var txt_location: UITextField!
    @IBOutlet var lbl_save_text: UILabel!
    @IBOutlet var btn_save_img: UIImageView!
    @IBOutlet var txt_timeAndDate: UITextField!
    @IBOutlet var img_back: UIImageView!
    @IBOutlet var btn_publicPrivate: UIButton!
    
    var selectedPin: MKPlacemark? //
    var datePicker : UIDatePicker!
    var teamPicker : UIPickerView!
    var arrayTeams = NSMutableArray()
    var arrayNFL = NSMutableArray()
    var arrayNBA = NSMutableArray()
    var arrayMLB = NSMutableArray()
    var arrayNCCA = NSMutableArray()
    var arrayImg = NSMutableArray()
    var checkWatchParty = Bool()
    var latitude_party = Double()
    var longitude_party = Double()
    var arraySelectedCell = NSMutableArray()
    var arrayCopy = NSMutableArray()
    var location_check = Bool()
    var timestamp = Int()
    
    var selectedTeam = String()
    var array_selectedTeam = NSMutableArray()
    var cell: Create_WatchParty_CollectionViewCell!
    var getNFL = NSMutableArray()
    var demoArray = NSMutableArray()
    var str = String()
    var arrayName = NSMutableArray()
    var arrayImages = NSMutableArray()
    var childKey = String()
    var checkParty = Bool()
    var team_b = Bool()
    var title_b = Bool()
    var time_b = Bool()
    var location_b = Bool()
    var childkeyConfirm = String()
    var deleteParty = Bool()
    var notAttending = Bool()
    var inviteAllFans = Bool()
    
    var strKey = String()
    var strTitle = String()
    var strTime = String()
    var strLocation = String()
    var strTeam = String()
    var loadingIndicator = UIActivityIndicatorView()
    var currentUID = Bool()
    var timeNdate = String()
    var resultSearchController: UISearchController!
    let locationManager = CLLocationManager()
    var userID = String()
    var checkPrivateOption = false
    var checkPublicOption = false
    var chatScreenBool = false
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var container: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var view_title: UIView!
    @IBOutlet weak var view_time: UIView!
    @IBOutlet weak var view_location: UIView!
    @IBOutlet weak var img_selectTeam: UIImageView!
    @IBOutlet weak var btn_inviteAllFans: UIButton!
//MARK: Back Button
    @IBAction func btn_back(_ sender: Any)
    {
        if deleteParty == true
        {
            self.deleteParty = false
        }
        self.navigationController?.popViewController(animated: true)
    }
//MARK: Location Button:-
    @IBAction func btn_gettingCurrentLocation(_ sender: Any)
    {
        self.tabBarController?.tabBar.isHidden = true;
     //   self.btn_save_img.isHidden = false
     //   self.btn_save_click.isHidden = false
     //   self.lbl_save_text.isHidden = false
        self.lbl_selectedTeam.isHidden = true
        self.img_back.isHidden = true
        self.btn_back.isHidden = true
        mapView.isHidden = false
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearch_TableViewController
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController.searchResultsUpdater = locationSearchTable
        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        self.mapView .addSubview(searchBar)

        searchBar.placeholder = "Search for places"
        container = resultSearchController?.searchBar
        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        locationSearchTable.mapView = mapView
        locationSearchTable.handleMapSearchDelegate = self
        self.indicator_location.isHidden = false
        self.indicator_location.startAnimating()
        self.btn_inviteAllFans.isUserInteractionEnabled = false

    }
//MARK: $$$ View Did Load :-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userID = (Auth.auth().currentUser?.uid)!
        self.tabBarController?.tabBar.isHidden = true
        
        view_title.layer.cornerRadius = 5
        view_title.layer.masksToBounds = true
        view_title.layer.borderWidth = 1.0
        view_title.layer.borderColor = UIColor.lightGray.cgColor
        view_time.layer.cornerRadius = 5
        view_time.layer.masksToBounds = true
        view_time.layer.borderWidth = 1.0
        view_time.layer.borderColor = UIColor.lightGray.cgColor
        view_location.layer.cornerRadius = 5
        view_location.layer.masksToBounds = true
        view_location.layer.borderWidth = 1.0
        view_location.layer.borderColor = UIColor.lightGray.cgColor
        img_selectTeam.layer.cornerRadius = self.img_selectTeam.frame.size.width/4
        img_selectTeam.layer.masksToBounds = true
        btn_inviteAllFans.layer.cornerRadius = self.btn_inviteAllFans.frame.size.height/2
        btn_inviteAllFans.layer.masksToBounds = true
        
        self.lbl_selectedTeam.text =    "Select Team"
        self.img_selectTeam.image = UIImage(named: "star_grey")
        self.btn_back.isHidden = false
    }
    
//MARK:~~~ View Will Appear:-
    override func viewWillAppear(_ animated: Bool)
    {
       // self.btn_publicPrivate.isHidden = false
        self.tabBarController?.tabBar.isHidden = true;
        self.btn_inviteAllFans.isHidden = true
        if checkParty == true // attending parting check
        {
            self.lbl_save_text.isHidden = true
            self.btn_save_click.isHidden = true
            self.btn_save_img.isHidden = true
            self.btn_publicPrivate.isHidden = true
            checkParty = false
            self.btn_back.isHidden = false
            self.selectInvitation()
        }
        if deleteParty == true  // delete party
        {
            self.lbl_save_text.isHidden = true
            self.btn_save_click.isHidden = false
            self.btn_save_img.isHidden = true
            self.btn_save_click.setImage(UIImage(named:"chat_1w"), for: UIControlState.normal)
            self.chatScreenBool = true          // for chat screen check
           // self.btn_save_img.image = UIImage(named:"chat_1w")
            self.btn_publicPrivate.isHidden = true
            self.btn_inviteAllFans.isHidden = false
            self.btn_back.isHidden = false
            self.cancelParty()
        }
        if notAttending == true     // not attending party check
        {
            self.btn_save_click.isHidden = false
            self.btn_save_img.isHidden = false
            self.chatScreenBool = true          // for chat screen check
            //self.btn_save_img.image = UIImage(named:"chat_1w")
            self.btn_save_click.setImage(UIImage(named:"chat_1w"), for: UIControlState.normal)
            
            self.btn_publicPrivate.isHidden = true
            self.lbl_save_text.isHidden = true
           // self.btn_save_click.isHidden = true
            self.btn_save_img.isHidden = true

            if (UserDefaults.standard.object(forKey: "checkUserIDForAttending") != nil)
            {
                self.currentUID = UserDefaults.standard.object(forKey: "checkUserIDForAttending") as! Bool
                self.strTeam = UserDefaults.standard.object(forKey: "attendingStrTeamValue") as! String
                self.strKey = UserDefaults.standard.object(forKey: "attendingStrKeyValue") as! String
                self.strTime = UserDefaults.standard.object(forKey: "attendingStrTimeValue") as! String
                self.strLocation = UserDefaults.standard.object(forKey: "attendingStrLocationValue") as! String
                self.strTitle = UserDefaults.standard.object(forKey: "attendingStrTitleValue") as! String
             }
            if currentUID == true
            {
                self.btn_inviteAllFans.isHidden = true
            }
            notAttending = false
            self.btn_back.isHidden = false
            self.cancelAttending()
        }
        if checkWatchParty == true
        {
            if self.btn_publicPrivate.titleLabel?.text != "Public"
            {
                self.btn_inviteAllFans.isHidden = false
                self.collectionView.isUserInteractionEnabled = true
            }else
                {
                    if lbl_selectedTeam.text != "Select Team" || lbl_selectedTeam.text == "Select Team"
                    {
                        self.inviteAllFans = true
                        self.btn_inviteAllFans.isHidden = true
                        self.collectionView.isUserInteractionEnabled = false
                        self.collectionView.reloadData()
                    }
                
                }
            
            self.btn_publicPrivate.isHidden = false
            self.arraySelectedCell.removeAllObjects()
            self.gettingUsers()
        }
        

    }
    //MARK:@@@ Getting users Here :-
    func gettingUsers()
    {
        loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        loadingIndicator.center = self.collectionView.center
        loadingIndicator.color = UIColor.black
        loadingIndicator.startAnimating()
        self.loadingIndicator.hidesWhenStopped = true
        self.collectionView.addSubview(self.loadingIndicator)
        
            if UserDefaults.standard.object(forKey: "selectedTeam") != nil
            {
                self.lbl_selectedTeam.text = UserDefaults.standard.object(forKey: "selectedTeam") as? String
                self.img_selectTeam.image = UIImage(named: "star_red")
            }
            self.demoArray.removeAllObjects()
            self.arrayCopy.removeAllObjects()
            self.arrayName.removeAllObjects()
            self.arrayImg.removeAllObjects()
            collectionView.isHidden = false
            let ref = Database.database().reference()
            ref.child("Favourites").child(self.lbl_selectedTeam.text!).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists()
                {
                    for a in ((snapshot.value as AnyObject).allKeys)!
                    {
                        self.str = a as! String
                        if self.str == self.userID
                        {
                            
                        }
                        else
                        {
                            self.demoArray.add(a)
                            print(a)
                            self.arrayCopy.add(a)
                            let ref1 = Database.database().reference()
                            ref1.child("users").child(self.str).observeSingleEvent(of: .value, with: { (snapshot) in
                                if snapshot.exists()
                                {
                                    var postDict = snapshot.value as! [String : AnyObject]
                                    let array = postDict["First Name"]
                                    self.arrayName.add(array!)
                                    let images = postDict["Profile Image"]
                                    self.arrayImages.add(images!)
                                    self.collectionView.reloadData()
                                    self.loadingIndicator.stopAnimating()
                                }
                                else
                                {
//                                    print(self.str)
//                                    ref.child("Favourites").child(self.lbl_selectedTeam.text!).child(self.str).removeValue()
//                                    if self.demoArray.contains(self.str)
//                                    {
//                                        self.demoArray.remove(self.str)
//                                    }
//                                    if self.arrayCopy.contains(self.str)
//                                    {
//                                        self.arrayCopy.remove(self.str)
//                                    }
                                }
                            })
                        }
                    }
                    self.checkWatchParty = false
                    self.collectionView.reloadData()
                    
                }
                else
                {
                    self.checkWatchParty = false
                    self.loadingIndicator.stopAnimating()
                    self.getNFL.removeAllObjects()
                    self.collectionView.reloadData()
                }
                })
    }
    
    //MARK: $$$ Confirm Invitation Here :-
    func selectInvitation()
    {
        self.btn_back.isHidden = false
        self.collectionView.isHidden = true
        self.btn_inviteAllFans.isHidden = true
        self.btn_location_click.isHidden = true
        txt_timeAndDate.isUserInteractionEnabled = false
        txt_title.isUserInteractionEnabled = false
        btn_selectTeam.isHidden = true
        txt_location.isUserInteractionEnabled = false
        
        childkeyConfirm = strKey
        self.txt_timeAndDate.text = strTime
        self.txt_location.text = strLocation
        self.txt_title.text = strTitle
        self.lbl_selectedTeam.text = strTeam
        self.img_selectTeam.image = UIImage(named: "star_red")
        self.lbl_watchParty.text = "Confirm Your Party"
        
        
        let btn_attending: UIButton = UIButton(frame: CGRect(x: self.view.frame.size.width/3, y:self.view.frame.size.height/2, width: self.view.frame.size.width/2.6, height: self.view.frame.size.height/20))
//        let btn_attending: UIButton = UIButton(frame: CGRect(x: self.view.frame.size.width/14, y:self.view.frame.size.height/2, width: self.view.frame.size.width/2.6, height: self.view.frame.size.height/20))

        btn_attending.setTitle("Attending", for: UIControlState.normal)
        btn_attending.backgroundColor = UIColor.red
        btn_attending.layer.cornerRadius = 5
        btn_attending.layer.masksToBounds = true
        btn_attending.layer.borderWidth = 1.0
        btn_attending.layer.borderColor = UIColor.lightGray.cgColor
        btn_attending.titleLabel?.minimumScaleFactor = 0.5
        btn_attending.titleLabel?.adjustsFontSizeToFitWidth = true
        self.collectionView.addSubview(btn_attending)
        self.view.addSubview(btn_attending)
        btn_attending.addTarget(self, action: #selector(Create_WatchParty_ViewController.invitationConfirm), for: UIControlEvents.touchUpInside)
        
    }
    //MARK: Confirm Invitation :-
    func invitationConfirm()
    {
        let ref = Database.database().reference()
        ref.child("Party").child(self.childkeyConfirm).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            if snapshot.exists()
            {
                ref.child("users").child(self.userID).child("Party").child("Invited").child(self.childkeyConfirm).removeValue(completionBlock: {  error in
                })
                ref.child("users").child(self.userID).child("Party").child("Attending").child(self.childkeyConfirm).updateChildValues(["Team": self.strTeam])
                ref.child("Party").child(self.childkeyConfirm).child("receiverID").child(self.userID).updateChildValues(["Team": self.strTeam])
                
                // user's points here added
                if UserDefaults.standard.object(forKey: "userPoints") != nil
                {
                    var points = UserDefaults.standard.object(forKey: "userPoints") as! Int
                    points += 5
                    ref.child("users").child(self.userID).updateChildValues(["Points":points])
                }
                
                let alert = UIAlertController(title: "Alert", message: "Party Confirmed", preferredStyle: UIAlertControllerStyle.alert)
                let confirm = UIAlertAction(title: "ok", style: UIAlertActionStyle.default)
                {
                    UIAlertAction in
                    self.navigationController?.popViewController(animated: true)
                  //  self.navigationController?.popToRootViewController(animated: true)
                }
                alert.addAction(confirm)
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: "Alert", message: "Party does not exist", preferredStyle: UIAlertControllerStyle.alert)
                let confirm = UIAlertAction(title: "ok", style: UIAlertActionStyle.default)
                {
                    UIAlertAction in
                    self.navigationController?.popToRootViewController(animated: true)
                }
                alert.addAction(confirm)
                self.present(alert, animated: true, completion: nil)

             }
        })
        
    }

//MARK: Not Attending Party:-
    func cancelAttending ()
    {
        self.btn_back.isHidden = false
        self.collectionView.isHidden = true
        self.btn_inviteAllFans.isHidden = true
        self.btn_location_click.isHidden = true
        txt_timeAndDate.isUserInteractionEnabled = false
        txt_title.isUserInteractionEnabled = false
        btn_selectTeam.isHidden = true
        txt_location.isUserInteractionEnabled = false
        
        childkeyConfirm = strKey
        self.txt_timeAndDate.text = strTime
        self.txt_location.text = strLocation
        self.txt_title.text = strTitle
        self.lbl_selectedTeam.text = strTeam
        self.img_selectTeam.image = UIImage(named: "star_red")
        self.lbl_watchParty.text = "Watch Party"

        let btn_NotAttending: UIButton = UIButton(frame: CGRect(x: self.view.frame.size.width/3, y:self.view.frame.size.height/2, width: self.view.frame.size.width/2.6, height: self.view.frame.size.height/20))
        btn_NotAttending.setTitle("Not Attending", for: UIControlState.normal)
        btn_NotAttending.backgroundColor = UIColor.red
        btn_NotAttending.layer.cornerRadius = 5
        btn_NotAttending.layer.masksToBounds = true
        btn_NotAttending.layer.borderWidth = 1.0
        btn_NotAttending.layer.borderColor = UIColor.lightGray.cgColor
        btn_NotAttending.titleLabel?.minimumScaleFactor = 0.5
        btn_NotAttending.titleLabel?.adjustsFontSizeToFitWidth = true
        if currentUID == true
        {
            btn_NotAttending.isHidden = true
            self.partyDetails()
        }
        else
        {
            btn_NotAttending.isHidden = false
        }
        self.collectionView.addSubview(btn_NotAttending)
        self.view.addSubview(btn_NotAttending)
        btn_NotAttending.addTarget(self, action: #selector(Create_WatchParty_ViewController.cancelAttendingConfirm), for: UIControlEvents.touchUpInside)
    }
//MARK: cancel attending Confirm:-
    func cancelAttendingConfirm()
    {
        let ref = Database.database().reference()
        ref.child("Party").child(childkeyConfirm).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            if snapshot.exists()
            {

                ref.child("Party").child(self.childkeyConfirm).child("receiverID").child(self.userID).removeValue(completionBlock: {  error in
                })
                ref.child("users").child(self.userID).child("Party").child("Attending").child(self.childkeyConfirm).removeValue(completionBlock: {  error in
                })
                ref.child("users").child(self.userID).child("Party").child("Invited").child(self.childkeyConfirm).updateChildValues(["Team": self.strTeam])
                
                let alert = UIAlertController(title: "Alert", message: "Not Attending", preferredStyle: UIAlertControllerStyle.alert)
                let confirm = UIAlertAction(title: "ok", style: UIAlertActionStyle.default)
                {
                    UIAlertAction in
                    self.navigationController?.popToRootViewController(animated: true)
                }
                alert.addAction(confirm)
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: "Alert", message: "Party does not exist", preferredStyle: UIAlertControllerStyle.alert)
                let confirm = UIAlertAction(title: "ok", style: UIAlertActionStyle.default)
                {
                    UIAlertAction in
                    self.navigationController?.popToRootViewController(animated: true)
                }
                alert.addAction(confirm)
                self.present(alert, animated: true, completion: nil)

            }
        })
        
    }
 
    
//MARK: Delete Party :-
     func cancelParty()
     {
        self.btn_back.isHidden = false
        self.collectionView.isHidden = false
        self.btn_location_click.isHidden = true
        txt_timeAndDate.isUserInteractionEnabled = false
        txt_title.isUserInteractionEnabled = false
        self.btn_inviteAllFans.setTitle("Delete", for: UIControlState.normal)
        btn_selectTeam.isHidden = true
        txt_location.isUserInteractionEnabled = false
        
        childkeyConfirm = strKey
        self.txt_timeAndDate.text = strTime
        self.txt_location.text = strLocation
        self.txt_title.text = strTitle
        self.lbl_selectedTeam.text = strTeam
        self.img_selectTeam.image = UIImage(named: "star_red")
        self.lbl_watchParty.text = "Delete Your Party"

        self.partyDetails()
     }
    
//MARK: Delete Party :-
    func partyDetails()
    {
        loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        loadingIndicator.center = self.collectionView.center
        loadingIndicator.color = UIColor.black
        loadingIndicator.startAnimating()
        self.loadingIndicator.hidesWhenStopped = true
        self.collectionView.isHidden = false
        self.collectionView.addSubview(self.loadingIndicator)
        
        self.arrayCopy.removeAllObjects()
        self.arrayName.removeAllObjects()
        self.arrayImg.removeAllObjects()
        let ref = Database.database().reference()
        ref.child("Party").child(childkeyConfirm).child("receiverID").observe( .value, with: { (snapshot) in
            if snapshot.hasChildren() == true
            {
            
                for a in ((snapshot.value as AnyObject).allKeys)!
                {
                    self.str = a as! String
                    print(self.str)
                    if self.str != self.userID
                    {
                        let ref1 = Database.database().reference()
                        ref1.child("users").child(self.str).observe( .value, with: { (snapshot) in
                        if snapshot.exists()
                            {
                                var postDict = snapshot.value as! [String : AnyObject]
                                let array = postDict["First Name"]
                                self.arrayName.add(array!)
                                let images = postDict["Profile Image"]
                                self.arrayImages.add(images!)
                                self.collectionView.reloadData()
                                self.loadingIndicator.stopAnimating()
                                Database.database().reference().removeAllObservers()
                            }
                            else
                            {
                                
                            }
                        })
                    }
                }
                self.collectionView.reloadData()
                Database.database().reference().removeAllObservers()
            }
            else
            {
                self.loadingIndicator.stopAnimating()
                self.arrayImages.removeAllObjects()
                self.arrayName.removeAllObjects()
                
                self.getNFL.removeAllObjects()
                self.collectionView.reloadData()
                Database.database().reference().removeAllObservers()

            }
        })

    }

    func getDirections()
    {
        guard let selectedPin = selectedPin else { return }
        let mapItem = MKMapItem(placemark: selectedPin)
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        mapItem.openInMaps(launchOptions: launchOptions)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//MARK: ### Collection View Cell Functions :-
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayName.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Create_WatchParty_CollectionViewCell
        cell.layer.cornerRadius = 5;
        cell.layer.masksToBounds = true
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.lbl.text = arrayName[indexPath.row] as? String
        let profileImg = arrayImages[indexPath.row] as? String
        cell.img.sd_setShowActivityIndicatorView(true)
        cell.img.sd_setIndicatorStyle(.gray)
        cell.img.sd_setImage(with: URL(string: profileImg!), placeholderImage: UIImage(named: "man_2"))
        
        if self.checkPrivateOption == true
        {
            if self.arraySelectedCell.contains(indexPath.row)
            {
                cell.layer.borderWidth = 1.0
                cell.layer.borderColor = UIColor.gray.cgColor
                self.arraySelectedCell.remove(indexPath.row)
            }
            let countCheck = self.arrayName.count-1
            print(countCheck)
            if indexPath.row == countCheck
            {
                self.checkPrivateOption = false
            }
            return cell
        }
        
        if self.checkPublicOption == true
        {
            if self.arraySelectedCell.contains(indexPath.row)
            {
                cell.layer.borderWidth = 1.3
                cell.layer.borderColor = UIColor.red.cgColor
            }
            else
            {
                cell.layer.borderWidth = 1.3
                cell.layer.borderColor = UIColor.red.cgColor
                self.arraySelectedCell.add(indexPath.row)
            }
            
            let countCheck = self.arrayName.count-1
            print(countCheck)
            if indexPath.row == countCheck
            {
                self.checkPublicOption = false
            }
            return cell
        }

        
         if self.inviteAllFans == true
         {
            cell.layer.borderWidth = 1.3
            cell.layer.borderColor = UIColor.red.cgColor
            if self.arraySelectedCell.contains(indexPath.row)
            {
                
            }
            else
            {
                self.arraySelectedCell.add(indexPath.row)
            }
            return cell
        }
       
        
        if self.arraySelectedCell.contains(indexPath.row)
        {
            cell.layer.borderWidth = 1.3
            cell.layer.borderColor = UIColor.red.cgColor
        }
       
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if deleteParty == true
        {
            
        }
        else
        {
            self.inviteAllFans = false
            let cell = collectionView.cellForItem(at: indexPath)
          
            if self.arraySelectedCell.contains(indexPath.row)
            {
                cell?.layer.borderWidth = 1.0
                cell?.layer.borderColor = UIColor.gray.cgColor
                self.arraySelectedCell.remove(indexPath.row)
            }
            else
            {
                self.arraySelectedCell.add(indexPath.row)
                cell?.layer.borderWidth = 1.3
                cell?.layer.borderColor = UIColor.red.cgColor
            }
        }
    }
    func chatScreenOpen()
    {
        UserDefaults.standard.set(self.strTitle, forKey: "partyName")
        UserDefaults.standard.set(self.strKey, forKey: "partyKey")
        //print("chat data : =\(self.strTitle)\(self.strKey)")
//        let notificationNameChat = Notification.Name("chatScreenNotification")
//        NotificationCenter.default.post(name: notificationNameChat, object: nil)
        
        self.tabBarController?.tabBar.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let walkthroughVC = storyboard.instantiateViewController(withIdentifier: "chatScreen") as! ChatScreen_ViewController
        self.navigationController?.pushViewController(walkthroughVC, animated: false)
    }
    
//MARK:###<<<<<<<<<<<< Save Button >>>>>>>>>>>>#########
    @IBAction func btn_saveButton_value(_ sender: Any)
    {
        if self.chatScreenBool == true
        {
            self.chatScreenOpen()
            //self.navigationController?.popViewController(animated: false)
           /* let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let walkthroughVC = storyboard.instantiateViewController(withIdentifier: "tabBar1") as! UITabBarController
            walkthroughVC.selectedIndex = 1
            UserDefaults.standard.set(true, forKey: "partyEventScreen")
           // let navigationController = self.view.window?.rootViewController as! UINavigationController
            self.navigationController?.pushViewController(walkthroughVC, animated: false)
           // navigationController.pushViewController(walkthroughVC, animated: false)*/
        }
        else
        {
        if mapView.isHidden == false
        {
            self.tabBarController?.tabBar.isHidden = true;
            mapView.isHidden = true
            self.img_back.isHidden = false
            self.btn_back.isHidden = false
          //  self.btn_save_click.isHidden = true
         //   self.btn_save_img.isHidden = true
        //    self.lbl_save_text.isHidden = true
            self.lbl_selectedTeam.isHidden = false
        }
        else{
        if lbl_selectedTeam.text != "Select Team" && txt_title.text != "" && txt_timeAndDate.text != "" && txt_location.text != "" /* && self.arraySelectedCell.count != 0*/
        {
                if self.demoArray.count == 0
                {
                    let alert = UIAlertController(title: "Alert", message: "Select Fans", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    self.btn_save_click.isUserInteractionEnabled = false
                    let ref = Database.database().reference()

                    // user's points here added
                    if UserDefaults.standard.object(forKey: "userPoints") != nil
                    {
                        var points = UserDefaults.standard.object(forKey: "userPoints") as! Int
                        points += 10
                        ref.child("users").child(userID).updateChildValues(["Points":points])
                    }
                   
                
                    let timestampp = NSDate().timeIntervalSince1970
                    let rest = Double(timestampp * 1000)
                    let timeStampFnl = Int64(rest)
                    
               
                    let checkPublic = self.btn_publicPrivate.titleLabel?.text!
                    
                    ref.child("Party").childByAutoId().updateChildValues(["Team_Name" : self.lbl_selectedTeam.text!, "Title" : self.txt_title.text!, "Location" : self.txt_location.text!,"location_laitude": latitude_party,"location_longitude": longitude_party, "Status" : (checkPublic)!, "Time" : self.txt_timeAndDate.text!,"TimeForTimeStamp": self.timeNdate, "userID": userID, "receiverID": "", "timeStamp": timeStampFnl],withCompletionBlock: { (error, reference) in
                        
                        self.childKey = reference.key
                        DispatchQueue.main.async {
                            print(self.arraySelectedCell.count)
                            if self.arraySelectedCell.count != 0
                            {
                                self.informUsers()
                            }
                            else
                            {
                                let teamName = UserDefaults.standard.object(forKey: "selectedTeam") as? String
                                ref.child("users").child(self.userID).child("Party").child("Yours").child(self.childKey).updateChildValues(["Team" : teamName!])
                            }
                        }
                        let alert = UIAlertController(title: "Alert", message: "Party Created", preferredStyle: UIAlertControllerStyle.alert)
                        let confirm = UIAlertAction(title: "ok", style: UIAlertActionStyle.default)
                        {
                            UIAlertAction in
                            self.btn_save_click.isUserInteractionEnabled = true
                            self.txt_location.text = ""
                            self.lbl_selectedTeam.text =    "Select Team"
                            self.img_selectTeam.image = UIImage(named: "star_grey")
                            self.txt_title.text = ""
                            self.txt_timeAndDate.text = ""
                            self.collectionView.isHidden = true
                            self.navigationController?.popToRootViewController(animated: true)
                        
                        }
                        alert.addAction(confirm)
                        self.present(alert, animated: true, completion: nil)
                    
                    })
                }
            }
        if lbl_selectedTeam.text == "Select Team"
        {
            let alert = UIAlertController(title: "Alert", message: "Select Team First", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        if txt_title.text == ""
        {
            let alert = UIAlertController(title: "Alert", message: "Enter Title for the Party", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        if txt_location.text == ""
        {
            let alert = UIAlertController(title: "Alert", message: "Select Party Location", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        if txt_timeAndDate.text == ""
        {
            let alert = UIAlertController(title: "Alert", message: "Select Time & Date for The Party", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
      /*  if self.arraySelectedCell.count == 0
        {
            let alert = UIAlertController(title: "Alert", message: "No Fan Selected for The Party", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)

        }*/
    }
    }
}
//MARK: text field functions
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUpDate(self.txt_timeAndDate)
    }
    private func textFieldDidEndEditing(_ textField: UITextField)-> Bool {
        
        return true;
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.view.frame.origin.y = 0;
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
//MARK:- Function of datePicker
    func pickUpDate(_ textField : UITextField){
        
//MARK: ^^^ DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 300))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
        self.datePicker.layer.masksToBounds = true
        self.datePicker.translatesAutoresizingMaskIntoConstraints = false
        textField.inputView = self.datePicker
        
// ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
       // toolBar.backgroundColor = UIColor.red
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
// Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(Create_WatchParty_ViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(Create_WatchParty_ViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
// Button Done and Cancel
    func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .long
        dateFormatter1.timeStyle = .short
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"    //"dd MMMM yyyy hh:mm a Z"
        self.timeNdate = dateFormatter.string(from: datePicker.date)
        txt_timeAndDate.text = dateFormatter1.string(from: datePicker.date)
       // print(self.txt_timeAndDate.text!)
        txt_timeAndDate.resignFirstResponder()
    }
    func cancelClick() {
        txt_timeAndDate.resignFirstResponder()
    }
    
//MARK: Select Teams :-
    @IBAction func select_Team(_ sender: Any)
    {
        self.checkWatchParty = true
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "favorites") as! Favorites_ViewController
        vc.checkFavorites = true
        self.navigationController!.pushViewController(vc, animated: true)
    }

//MARK: Public / Praviate option
    
    @IBAction func btn_publicPrivate(_ sender: Any)
    {
       // DispatchQueue.main.async {
            
        if self.btn_publicPrivate.titleLabel?.text == "Public"
        {
            self.btn_publicPrivate.setTitle("Private", for: UIControlState.normal)
            self.btn_publicPrivate.setImage(UIImage(named: "white_star1"), for: UIControlState.normal)
            self.btn_inviteAllFans.isHidden = false
            self.collectionView.isUserInteractionEnabled = true
            self.inviteAllFans = false
            self.checkPrivateOption = true
            self.collectionView.reloadData()
        }
        else
        {
            self.btn_publicPrivate.setTitle("Public", for: UIControlState.normal)
            self.btn_publicPrivate.setImage(UIImage(named: "star"), for: UIControlState.normal)
            self.btn_inviteAllFans.isHidden = true
            if self.lbl_selectedTeam.text != "Select Team"
            {
                self.inviteAllFans = false
                self.checkPublicOption = true
                
                self.collectionView.isUserInteractionEnabled = false
                self.collectionView.reloadData()
            }
        }
       // }
    }
    
//MARK: $$$$$$$$$ <<<< InviteAllFans >>>>>>> $$$$$$$$$$$
    @IBAction func inviteAllFans(_ sender: Any)
    {
        if deleteParty == true
        {
            let alert = UIAlertController(title: nil, message: "Do you really want to delete your party?", preferredStyle: UIAlertControllerStyle.alert)
            let confirm = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                self.deleteParty = false
                let ref = Database.database().reference()
                ref.child("Party").child(self.childkeyConfirm).removeValue(completionBlock: {  error in
                    print(error)
                })
                
                ref.child("users").child(self.userID).child("Party").child("Yours").child(self.childkeyConfirm).removeValue(completionBlock: {  error in
                    print(error)
                })

                self.navigationController?.popToRootViewController(animated: true)

            }
            let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
            }
            alert.addAction(confirm)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)

        }
        else if lbl_selectedTeam.text != "Select Team" && txt_title.text != "" && txt_timeAndDate.text != "" && txt_location.text != ""
        {
            self.inviteAllFans = true
          //  self.btn_save_click.isHidden = false
            
            
            self.collectionView.reloadData()

//            if self.demoArray.count == 0
//            {
//                self.demoArray.addObjects(from: self.arraySelectedCell as! [Any])
//            }
        }
     /*       if btn_inviteAllFans.titleLabel?.text == "Invite All Fans"
            {
                self.btn_inviteAllFans.titleLabel?.text = "Save & Invite"
                 self.btn_inviteAllFans.setTitle("Save & Invite", for: UIControlState.normal)
             
                self.demoArray.addObjects(from: self.arraySelectedCell as! [Any])

                
                self.collectionView.reloadData()
            }
        else
        {
            if self.demoArray.count == 0
            {
                self.demoArray.addObjects(from: self.arraySelectedCell as! [Any])
            }
            let ref = FIRDatabase.database().reference()
            let userID = FIRAuth.auth()?.currentUser?.uid

            let timestampp = NSDate().timeIntervalSince1970
            timestamp = Int(timestampp)
            ref.child("Party").childByAutoId().updateChildValues(["Team_Name" : self.lbl_selectedTeam.text!, "Title" : self.txt_title.text!, "Location" : self.txt_location.text!,"location_laitude": latitude_party,"location_longitude": longitude_party, "Status" : "", "Time" : self.txt_timeAndDate.text!, "userID": userID!, "receiverID": "", "timeStamp": timestamp],withCompletionBlock: { (error, reference) in
                self.childKey = reference.key
                self.informUsers()
            
                let alert = UIAlertController(title: "Alert", message: "Party Created", preferredStyle: UIAlertControllerStyle.alert)
                let confirm = UIAlertAction(title: "ok", style: UIAlertActionStyle.default)
                {
                    UIAlertAction in
                    self.txt_location.text = ""
                    self.lbl_selectedTeam.text =    "Select Team"
                    self.img_selectTeam.image = UIImage(named: "star_grey")
                    self.txt_title.text = ""
                    self.txt_timeAndDate.text = ""
                    self.collectionView.isHidden = true
                    self.navigationController?.popToRootViewController(animated: true)

                }
                alert.addAction(confirm)
                self.present(alert, animated: true, completion: nil)

            })
         }
        }*/
        if lbl_selectedTeam.text == "Select Team"
        {
            let alert = UIAlertController(title: "Alert", message: "Select Team First", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
       
        if txt_title.text == ""
        {
                let alert = UIAlertController(title: "Alert", message: "Enter Title for the Party", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
        if txt_location.text == ""
        {
                let alert = UIAlertController(title: "Alert", message: "Select Party Location", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
        if txt_timeAndDate.text == ""
        {
                let alert = UIAlertController(title: "Alert", message: "Select Time & Date for The Party", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
      

    }
    func informUsers()
    {
        if self.arraySelectedCell.count != 0
        {
            self.demoArray.removeAllObjects()
            for j in 0..<self.arraySelectedCell.count
            {
                self.demoArray.addObjects(from: [self.arrayCopy[self.arraySelectedCell[j] as! Int]])
                print(demoArray)
            }
        }
        
        if demoArray.count != 0
        {
            for i in 0..<self.demoArray.count
            {
                str = demoArray[i] as! String
                print(str)
                let ref1 = Database.database().reference()
                let teamName = UserDefaults.standard.object(forKey: "selectedTeam") as? String
                ref1.child("users").child(str).child("Party").child("Invited").child(self.childKey).updateChildValues(["Team" : teamName!])
                
                ref1.child("Party").child(self.childKey).child("receiverID").child((Auth.auth().currentUser?.uid)!).updateChildValues(["Team" : teamName!])
                
            ref1.child("users").child((Auth.auth().currentUser?.uid)!).child("Party").child("Yours").child(self.childKey).updateChildValues(["Team": teamName!])
                 ref1.child("users").child((Auth.auth().currentUser?.uid)!).child("Party").child("Attending").child(self.childKey).updateChildValues(["Team": teamName!])
            }
            self.InviteBadge()
        }
    }
    func InviteBadge()
    {
        for i in 0..<self.demoArray.count
         {
            str = demoArray[i] as! String
            let refo = Database.database().reference()
            refo.child("users").child(self.str).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists()
                {
                    let postDict = snapshot.value as! [String : AnyObject]
                    
                    // Creating InviteBadges here !!!
                    let roomKeys = Array(postDict.keys)
                    if roomKeys.contains("InviteBadge")
                    {
                        var count = postDict["InviteBadge"] as! Int
                        count = count + 1
                        refo.child("users").child(self.str).updateChildValues(["InviteBadge": count])
                    }
                    else
                    {
                        refo.child("users").child(self.str).updateChildValues(["InviteBadge": 0]) // added inviteBadge = 0
                    }
                    
                }else
                {
                    
                }
                
            }) { (error) in
                print(error.localizedDescription)
                
            }
        }
    }

    func timeString(time:TimeInterval) -> String {
        
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)

    }

 }
//MARK: ^^^^^^^^^^^^^^^$$$$$$$$$$$$$$$$^^^^^^^^^^^^^Time stamp
extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}

@available(iOS 9.0, *)
extension Create_WatchParty_ViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if location_check == false
        {
        guard let location = locations.first else { return }
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location.coordinate, span: span)
        mapView.setRegion(region, animated: true)
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            print(location)
            
            if error != nil {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0
            {
                let pm = placemarks?[0]
                // Location name
                let locationName = pm?.addressDictionary!["SubLocality"] as? NSString
                
                // Street address
                let city = pm?.addressDictionary!["City"] as? NSString
                
                // City
                let state = pm?.addressDictionary!["State"] as? NSString
            self.latitude_party = location.coordinate.latitude
            self.longitude_party = location.coordinate.longitude
            self.txt_location.text = "\(String(describing: locationName!))\(String(describing: city!))\(String(describing: state!))"
                self.indicator_location.stopAnimating()
                self.indicator_location.isHidden = true

                self.btn_inviteAllFans.isUserInteractionEnabled = true
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
        locationManager.stopUpdatingLocation()
        }
        else
        {
            self.indicator_location.stopAnimating()

        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error)")
    }
}

@available(iOS 9.0, *)
extension Create_WatchParty_ViewController: HandleMapSearch {
    
    func dropPinZoomIn(_ placemark: MKPlacemark){
        // cache the pin
        selectedPin = placemark
        location_check = true
        // clear existing pins
        mapView.removeAnnotations(mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        if let city = placemark.locality,
            let state = placemark.administrativeArea
            {
                annotation.subtitle = "\(city) \(state)"
            }
        latitude_party = placemark.coordinate.latitude
        longitude_party = placemark.coordinate.longitude
        
        let placename = placemark.name
        let placelocality = placemark.locality
        var finalplace = ""
        if(placename != nil)
        {
        finalplace = placename!
        } else
        {
        finalplace = ""
        }
        if(placelocality != nil)
        {
            if(finalplace != "")
            {
        finalplace = "\(finalplace)\(",")\(placelocality!)"
            } else {
            finalplace = "\(String(describing: placelocality))"
            }
        } else {
        }
        
        self.txt_location.text = "\(finalplace)"
        self.indicator_location.stopAnimating()
        self.indicator_location.isHidden = true
        self.btn_inviteAllFans.isUserInteractionEnabled = true

        mapView.addAnnotation(annotation)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(placemark.coordinate, span)
        mapView.setRegion(region, animated: true)
    }
    
}

@available(iOS 9.0, *)
extension Create_WatchParty_ViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        
        guard !(annotation is MKUserLocation) else { return nil }
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        }
        pinView?.pinTintColor = UIColor.red
        pinView?.canShowCallout = true
        let smallSquare = CGSize(width: 30, height: 30)
        let button = UIButton(frame: CGRect(origin: CGPoint.zero, size: smallSquare))
        button.setBackgroundImage(UIImage(named: "location-1"), for: UIControlState())
       // button.addTarget(self, action: #selector(Create_WatchParty_ViewController.getDirections), for: .touchUpInside)
        pinView?.leftCalloutAccessoryView = button
        
        return pinView
    }
}
