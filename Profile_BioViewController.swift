//
//  Profile_BioViewController.swift
//  FanClub
//
//  Created by Brst on 5/16/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseCore

class Profile_BioViewController: UIViewController {

    @IBOutlet weak var vw: UIView!
    @IBOutlet var txt_bio: UITextView!
    
    var checkProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if let tblHeightCheck =  UserDefaults.standard.object(forKey: "check") as? CGFloat
//        {
//            self.txt_bio.frame.size.height = tblHeightCheck
//            self.vw.frame.size.height = CGFloat(tblHeightCheck - 30)
//        }
        vw.layer.cornerRadius = 5;
        vw.layer.masksToBounds = true
        vw.layer.borderWidth = 1.0
        vw.layer.borderColor = UIColor.lightGray.cgColor
        
        vw.layer.masksToBounds = false
        vw.layer.shadowColor = UIColor.black.cgColor
        vw.layer.shadowOpacity = 0.1
        vw.layer.shadowOffset = CGSize(width: 0, height: 0)
        vw.layer.shadowRadius = 1
        
        let notificationName = Notification.Name("checkProfile")
        NotificationCenter.default.addObserver(self, selector: #selector(Profile_BioViewController.bio), name: notificationName, object: nil)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        //MARK: Fetching data from Database :
        self.bio()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func bio()
    {
        if UserDefaults.standard.object(forKey: "checkProfile") != nil
        {
            self.checkProfile = UserDefaults.standard.object(forKey: "checkProfile") as! Bool
        }
        if UserDefaults.standard.object(forKey: "bio") != nil && self.checkProfile == false
        {
            let bio = UserDefaults.standard.object(forKey: "bio") as! String
            
            
            self.txt_bio.text = bio
            
        }
        else
        {
            var userID = (Auth.auth().currentUser?.uid)!
            if self.checkProfile == true
            {
                userID = UserDefaults.standard.object(forKey: "userId") as! String
            }
            let refo = Database.database().reference()
            _ = refo.child("users").child(userID).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                if snapshot.exists()
                {
                    let postDict = snapshot.value as! [String : AnyObject]
                    
                    let bio = postDict["Bio"]
                    
                    self.txt_bio.text = bio as? String
                }
                
            })
            { (error) in
                print(error.localizedDescription)
            }
        }

    }
}
